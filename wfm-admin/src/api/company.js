import request from '@/utils/request'



/**
 * admin 新增一个用户
 * @param {*} user
 */
export function addCompany(data) {
  return request({
    url: '/company/createCompany',
    method: 'post',
    data
  })
}





/**
 * admin 新增一个用户
 * @param {*} user
 */
export function updateAttendance(data) {
    return request({
      url: '/company/updateAttendance',
      method: 'post',
      data
    })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function addAttendance(data) {
  return request({
    url: '/company/createAttendance',
    method: 'post',
    data
  })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function updateCompany(data) {
    return request({
      url: '/company/createCompany',
      method: 'post',
      data
    })
}

/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
export function getCompanyList(id) {
  return request({
    url: `/company/getCompanyList`,
    method: 'get',
     })
}


/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
export function getCompany(id) {
  return request({
    url: `/company/getCompany/${id}`,
    method: 'get',
     })
}


/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
export function getAttendaceList(id) {
  return request({
    url: `/company/getAttendaceList`,
    method: 'get',
     })
}


/**
 * 更新用户状态
 * @param {}} user
 */
export function updateUserStatus(data) {
  return request({
    url: '/company/status',
    method: 'put',
    data
  })
}


/**
 * 更新用户状态
 * @param {}} user
 */
// export function joinCompany(id) {
//   return request({
//     url: '/company/joinCompany/${id}',
//     method: 'get'
 
//   })
// }

/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
export function joinCompany(id) {
  return request({
    url: `/company/joinCompany/${id}`,
    method: 'get',
     })
 }

/**
 * 删除
 * @param {*} id
 */
// export function dotask(data) {
//   return request({
//     url: `/company/doTask`,
//     method: 'post',
//     data
//    })
// }



