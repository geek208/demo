import request from '@/utils/request'

export function getRoutes() {
  return request({
    url: '/routes',
    method: 'get'
  })
}

export function notify() {
  return request({
    url: '/notify',
    method: 'get'
  })
}

export function pay(data) {
  return request({
    url: '/pay',
    method: 'post',
    data
  })
}


export function createOrder(data) {
  return request({
    url: '/pay/createOrder',
    method: 'post',
    data
  })
}


export function getPayStatus(data) {
    return request({
      url: '/pay/getPayStatus',
      method: 'get',
      data
    })
  }
/**
 * admin 获取user列表
 * @param {} pageable
 */
export function orderList(pageable) {
  return request({
    url: '/orderList',
    method: 'get',
    params: pageable
  })
}


/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
 export function listDoneTask(page) {
  return request({
    url: `/pay/getEasyPayList`,
    method: 'get',
    params: page
  })
}



export function updateOrder(id, data) {
  return request({
    url: `/Order/${id}`,
    method: 'put',
    data
  })
}

export function deleteOrder(id) {
  return request({
    url: `/Order/${id}`,
    method: 'delete'
  })
}
