import request from '@/utils/request'



/**
 * admin 新增一个用户
 * @param {*} user
 */
export function createInsurance(data) {
  return request({
    url: '/company/createInsurance',
    method: 'post',
    data
  })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function createInsurancePlan(data) {
  return request({
    url: '/company/createInsurancePlan',
    method: 'post',
    data
  })
}

/**
 * admin 新增一个用户
 * @param {*} user
 */
export function createTaxPlan(data) {
  return request({
    url: '/company/createTaxPlan',
    method: 'post',
    data
  })
}

/**
 * admin 新增一个用户
 * @param {*} user
 */
export function updateTaxPlan(data) {
  return request({
    url: '/company/updateTaxPlan',
    method: 'post',
    data
  })
}



/**
 * admin 新增一个用户
 * @param {*} user
 */
export function updateInsurancePlan(data) {
  return request({
    url: '/company/updateInsurancePlan',
    method: 'post',
    data
  })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function updatensurance(data) {
    return request({
      url: '/company/createInsurance',
      method: 'post',
      data
    })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function addAttendance(data) {
  return request({
    url: '/company/createAttendance',
    method: 'post',
    data
  })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function updateCompany(data) {
    return request({
      url: '/company/updateCompany',
      method: 'post',
      data
    })
}

/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
export function getinsuranceplanList(id) {
  return request({
    url: `/company/getinsuranceplanList`,
    method: 'get',
     })
}


/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
export function getinsuranceList(id) {
  return request({
    url: `/company/getinsuranceList`,
    method: 'get',
     })
}



/**
 * 更新用户状态
 * @param {}} user
 */
export function updateUserStatus(data) {
  return request({
    url: '/company/status',
    method: 'put',
    data
  })
}


/**
 * 更新用户状态
 * @param {}} user
 */
// export function joinCompany(id) {
//   return request({
//     url: '/company/joinCompany/${id}',
//     method: 'get'
 
//   })
// }

/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
export function joinCompany(id) {
  return request({
    url: `/company/joinCompany/${id}`,
    method: 'get',
     })
 }

/**
 * 删除
 * @param {*} id
 */
// export function dotask(data) {
//   return request({
//     url: `/company/doTask`,
//     method: 'post',
//     data
//    })
// }



