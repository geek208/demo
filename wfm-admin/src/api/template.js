import request from '@/utils/request'

export function postfields(data) {
  return request({
    url: '/wfwactivity/apply',
    method: 'post',
    data
  })
}

/**
 * admin 新增一个用户
 * @param {*} user
 */
export function addTemplate(data) {
  return request({
    url: '/salary/createTemplate',
    method: 'post',
    data
  })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function updateTemplate(data) {
    return request({
      url: '/salary/updateTemplate',
      method: 'post',
      data
    })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function createItem(data) {
  return request({
    url: '/salary/createItem',
    method: 'post',
    data
  })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function updateItem(data) {
    return request({
      url: '/salary/updateSalaryItem',
      method: 'post',
      data
    })
}

/**
 * admin 新增一个用户
 * @param {*} user
 */
export function createColumn(data) {
  return request({
    url: '/salaryitem/createColumn',
    method: 'post',
    data
  })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function updateColumn(data) {
    return request({
      url: '/salaryitem/updateColumn',
      method: 'post',
      data
    })
}

export function getColumn(id) {
  return request({
    url: `/salaryitem/getColumn/${id}`,
    method: 'get'
  })
}

/**
 * 更新
 * @param {} data
 */
export function updateMessage(data) {
  return request({
    url: 'message',
    method: 'put',
    data
  })
}


/**
 * 更新
 * @param {} data
 */
 export function updateUserFields(data) {
  return request({
    url: '/wfwuser/updateUserFields',
    method: 'post',
    data
  })
}


/**
 * 获取一个server
 * @param {} id
 */
export function getMessage(id) {
  return request({
    url: `message/${id}`,
    method: 'get'
  })
}

/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
export function startprocess(id) {
  return request({
    url: `/salary/startProcess/${id}`,
    method: 'get',
  })
}


export function getProcess(id) {
  return request({
    url: `/salary/getProcess/${id}`,
    method: 'get',
  })
}


export function listusersalary(id) {
  return request({
    url: `/salary/listusersalary/${id}`,
    method: 'get',
  })
}

/**
 * list
 * @param {} id
 */
 export function listFlow(id) {
  return request({
    url: `/salary/getTemplateList/${id}`,
    method: 'get',
   })
}


export function listSalaryProcess() {
  return request({
    url: `/salary/listSalaryProcess`,
    method: 'get',
   })
}


/**
 * list
 * @param {} id
 */
 export function listUser(id) {
  return request({
    url: `/salary/getSalaryUserList/${id}`,
    method: 'get',
   })
}

/**
 * list
 * @param {} id
 */
 export function getUserFieldList(id) {
  return request({
    url: `/user/getUserFieldList/${id}`,
    method: 'get',
   })
}


/**
 * 删除
 * @param {*} id
 */
export function deleteMessage(id) {
  return request({
    url: `/salary/delProcess/${id}`,
    method: 'get'
  })
}


/**
 * list
 * @param {} id
 */
export function getColumnList(id) {
  return request({
    url: `/salaryitem/getColumnList3/${id}`,
    method: 'get',
   })
}
