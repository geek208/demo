import request from '@/utils/request'



/**
 * admin 新增一个用户
 * @param {*} user
 */
export function addApp(data) {
  return request({
    url: '/app/createApp',
    method: 'post',
    data
  })
}


/**
 * admin 新增一个用户
 * @param {*} user
 */
export function updateApp(data) {
    return request({
      url: '/app/updateApp',
      method: 'post',
      data
    })
}


/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
export function getAppList(id) {
  return request({
    url: `/app/getAppList`,
    method: 'get',
     })
}


/**
 * list
 * @param {int}} page
 * @param {int} pageSize
 */
export function getApp(id) {
  return request({
    url: `/app/getApp/${id}`,
    method: 'get',
     })
}



/**
 * 更新用户状态
 * @param {}} user
 */
export function updateUserStatus(data) {
  return request({
    url: '/user/status',
    method: 'put',
    data
  })
}
