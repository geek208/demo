/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const nestedRouter = {
  path: '/system',
  component: Layout,
  redirect: '/system/app/list',
  name: 'Nested',
  meta: {
    title: '系统管理',
    icon: 'nested'
  },
  children: [
    {
      path: 'app',
      component: () => import('@/views/nested/menu1/index'), // Parent router-view
      name: 'app',
      meta: { title: '应用管理' },
      // redirect: '/nested/menu1/menu1-1',
      children: [
        {
          path: 'create',
          component: () => import('@/views/app/create'),
          name: 'create',
          meta: { title: '应用注册' }
        },
        {
          path: 'list',
          component: () => import('@/views/app/list'),
          name: 'list',
          // redirect: '/nested/menu1/menu1-2/menu1-2-1',
          meta: { title: '应用列表' },
          // children: [
          //   {
          //     path: 'menu1-2-1',
          //     component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
          //     name: 'Menu1-2-1',
          //     meta: { title: 'Menu 1-2-1' }
          //   },
          //   {
          //     path: 'menu1-2-2',
          //     component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
          //     name: 'Menu1-2-2',
          //     meta: { title: 'Menu 1-2-2' }
          //   }
          // ]
        },
        {
          path: 'edit/:id(\\d+)',
          component: () => import('@/views/app/edit'),
          name: '应用编辑',
          meta: { title: '应用编辑' , roles: ['admin'], icon: 'edit' },
          hidden: true
        },
        // {
        //   path: 'menu1-3',
        //   component: () => import('@/views/nested/menu1/menu1-3'),
        //   name: 'Menu1-3',
        //   meta: { title: 'Menu 1-3' }
        // }
      ]
    },
    {
      path: 'company',
      name: 'company',
      component: () => import('@/views/nested/menu2/index'),
      meta: { title: '企业管理' },
      children: [
        {
          path: 'create',
          component: () => import('@/views/company/create'),
          name: 'create',
          meta: { title: '企业注册' }
        },
        {
          path: 'list',
          component: () => import('@/views/company/list'),
          name: 'list',
          meta: { title: '企业列表' }
        },
        {
          path: 'edit/:id(\\d+)',
          component: () => import('@/views/company/edit'),
          name: '企业编辑',
          meta: { title: '企业编辑' , roles: ['admin'], icon: 'edit' },
          hidden: true
        },
        {
          path: 'createItem/:id(\\d+)',
          component: () => import('@/views/company/createAttendance'),
          name: '登记考勤',
          meta: { title: '登记考勤' , roles: ['admin'] },
          hidden: true
        },
        {
          path: 'listAttendance', 
          component: () => import('@/views/company/listAttendance'),
          name: '考勤列表',
          meta: { title: '考勤列表' , roles: ['admin'] },
          //hidden: true
        },
      ]
      
    },
    {
      path: 'user',
      name: 'user',
      component: () => import('@/views/nested/menu2/index'),
      meta: { title: '用户管理' },
      children: [
        {
          path: 'create',
          component: () => import('@/views/company/create'),
          name: 'create',
          meta: { title: '企业注册' }
        },
        {
          path: 'list',
          component: () => import('@/views/company/list'),
          name: 'list',
          meta: { title: '企业列表' }
        },
        {
          path: 'edit/:id(\\d+)',
          component: () => import('@/views/company/edit'),
          name: '企业编辑',
          meta: { title: '企业编辑' , roles: ['admin'], icon: 'edit' },
          hidden: true
        },
        {
          path: 'createItem/:id(\\d+)',
          component: () => import('@/views/company/createAttendance'),
          name: '登记考勤',
          meta: { title: '登记考勤' , roles: ['admin'] },
          hidden: true
        },
        {
          path: 'listAttendance', 
          component: () => import('@/views/company/listAttendance'),
          name: '考勤列表',
          meta: { title: '考勤列表' , roles: ['admin'] },
          //hidden: true
        },
      ]
      
    }
  ]
}

export default nestedRouter
