package com.hadron;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.google.common.collect.Lists;
import com.hadron.wfw.api.AuthInterceptor;


@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {
	@Autowired
	AuthInterceptor authInterceptor;
//	@Autowired
//	ProxyConstant proxyConstant;

	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(0, fastJsonHttpMessageConverter());

	}

	@Bean
	public FastJsonHttpMessageConverter fastJsonHttpMessageConverter() {
		FastJsonHttpMessageConverter fastJsonHttpMessageConverter = new FastJsonHttpMessageConverter();
		fastJsonHttpMessageConverter.setSupportedMediaTypes(Lists.newArrayList(MediaType.APPLICATION_JSON));
		return fastJsonHttpMessageConverter;
	}

	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(fastJsonHttpMessageConverter());
		restTemplate.getInterceptors().add((request, body, execution) -> {
			HttpHeaders headers = request.getHeaders();
			//headers.add("Authorization", DigestUtils.md5Hex(proxyConstant.getAuthPassword()));
			headers.setContentType(MediaType.APPLICATION_JSON);
			return execution.execute(request, body);
		});
		return restTemplate;
	}



	/**
	 * 拦截器
	 *
	 * @param registry
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(authInterceptor).addPathPatterns("/**");
	}

	@Override
	public void addCorsMappings(CorsRegistry corsRegistry) {
		/**
		 * 所有请求都允许跨域，使用这种配置就不需要 在interceptor中配置header了
		 */
		corsRegistry.addMapping("/**")
		        .allowCredentials(true)
//				.allowedOrigins("http://192.168.1.192:9527", "http://192.168.1.116:9527", "http://127.0.0.1:9527",
//						"https://zfm.nnt.ltd/pay", "http://192.168.1.114:80", "http://34.80.7.198:80",
//						"http://192.168.1.192:9526", "http://34.80.7.198:9526", "http://192.168.1.114:9526",
//						"http://34.96.205.232:9526")
		        .allowedOrigins("*")
		        .allowedMethods("*")
		        .allowedHeaders("*")
//				.allowedMethods("POST", "GET", "PUT", "OPTIONS", "DELETE").allowCredentials(true).allowedHeaders("*")
				.maxAge(3600);
	}

	/**
	 * Long ->String
	 *
	 */
	/*
	 * @Override public void
	 * configureMessageConverters(List<HttpMessageConverter<?>> converters) {
	 * MappingJackson2HttpMessageConverter messageConverter = new
	 * MappingJackson2HttpMessageConverter(); ObjectMapper mapper = new
	 * ObjectMapper(); Hibernate5Module module = new Hibernate5Module();
	 * module.enable(Hibernate5Module.Feature.FORCE_LAZY_LOADING);
	 * mapper.registerModule(module); messageConverter.setObjectMapper(mapper);
	 * converters.add(messageConverter); }
	 */

	/*
	 * @Override public void addResourceHandlers(ResourceHandlerRegistry
	 * registry) { registry.addResourceHandler("swagger-ui.html")
	 * .addResourceLocations("classpath:/META-INF/resources/");
	 * registry.addResourceHandler("/webjars/**")
	 * .addResourceLocations("classpath:/META-INF/resources/webjars/");
	 * 
	 * }
	 */

}
