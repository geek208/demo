package com.hadron;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import com.alibaba.fastjson.JSON;
import com.hadron.app.SalaryAttendanceRepository;
import com.hadron.app.SalaryColumnCodeRepository;
import com.hadron.app.SalaryInsuranceRepository;
import com.hadron.wfw.model.SalaryColumnCode;
import com.hadron.wfw.service.UserRepository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.Date;
import java.util.Map;

/**
 * @author tqf
 * @Description 项目启动初始化数据
 * @Version 1.0
 * @since 2022-04-13 09:29
 */
@Configuration
@Slf4j
public class DataInitializationConfig {

//    @Autowired
    DataSource dataSource;
	@Autowired
	private  SalaryColumnCodeRepository salaryColumnCodeRepository;
	@Autowired
	private  SalaryInsuranceRepository salaryInsuranceRepository;
	
	@Autowired
	private SalaryAttendanceRepository salaryAttendanceRepository;
	
	@Autowired
    UserRepository userRepository;
	

    @PostConstruct
    public void init() {
        // 项目启动初始化基本数据
        ///log.info("数据初始化开始: " + DateUtil.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss"));
        // 通过直接读取sql文件执行
//        Resource resources = new ClassPathResource("sql/client_api_auth.sql");
//        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
//        resourceDatabasePopulator.addScripts(resources);
//        resourceDatabasePopulator.execute(dataSource);
        //log.info("数据初始化结束: " + DateUtil.dateToString(new Date(),"yyyy-MM-dd HH:mm:ss"));
    	
        //String json = salaryInsuranceRepository.findById(1582).getJson();
    	
    	//String json = userRepository.findById(1488).getJson();
    	
//        String json = salaryAttendanceRepository.findById(1673).getJson();
//		
//		Map maps2 = JSON.parseObject(json,Map.class);
//        System.out.println("使用JSON类，指定解析类，解析json字符串：");
//        for (Object key:maps2.keySet()) {
//            System.out.println("key:" + key + "->value:" + maps2.get(key));
//            SalaryColumnCode column  =new  SalaryColumnCode();
//            
//            column.setColumnName(key.toString());
//            column.setCnName("工资");
//            column.setDataType("3");
//            
//            //salaryColumnCodeRepository.save(column);
//          } 
    }
}