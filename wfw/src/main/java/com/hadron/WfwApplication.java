package com.hadron;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.hadron.app.SalaryColumnCodeRepository;
import com.hadron.app.SalaryInsuranceRepository;
import com.hadron.wfw.api.UserController;
import com.hadron.wfw.model.SalaryColumnCode;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author xuychao
 * @date 2022年3月15日
 * @classname WfwApplication.java
 * @email xuychao@163.com  git@github.com:geek208/wfw.git
 */
//@EnableOAuth2Sso
@SpringBootApplication
//@RestController
@Slf4j
public class WfwApplication {


	public static void main(String[] args) {
		SpringApplication.run(WfwApplication.class, args);
		//init();
		
	}
	
//	   // sso测试接口
//    @GetMapping("/user")
//    public Authentication getUser(Authentication authentication) {
//        log.info("auth : {}", authentication);
//        System.err.println("auth: user {}"+ authentication.getDetails().toString());
//        return authentication;
//
//    }
	

	}

