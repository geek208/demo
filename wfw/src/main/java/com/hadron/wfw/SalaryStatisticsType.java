package com.hadron.wfw;


/**
 * 
 * @author xuychao
 * @date 2022年3月15日
 * @classname ActionType.java
 * @email xuychao@163.com  git@github.com:geek208/wfw.git
 */

public enum SalaryStatisticsType {
	
	//未支付 0,支付成功 1,支付失败 2,待退款 3,已退款 4

	totalWagePayable("totalWagePayable", "应发合计"), 
	totalDeduct("totalDeduct", "应扣合计"),
	wagePayable("totalWagePayable", "应发工资"), 
	netPayroll("netPayroll", "实发工资"),
	totalTax("totalTax", "个人所得税"),
	taxPayable("taxPayable", "应税工资"),
	dayPay("dayPay", "日薪"),
	hourPay("hourPay", "时薪"),
	overtimesalary("overtimesalary", "加班工资");

    private String value;
    private String desc;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private SalaryStatisticsType(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}