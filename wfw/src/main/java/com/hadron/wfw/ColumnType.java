package com.hadron.wfw;


/**
 * 
 * @author xuychao
 * @date 2022年3月15日
 * @classname ActionType.java
 * @email xuychao@163.com  git@github.com:geek208/wfw.git
 */

public enum ColumnType {
	
	//未支付 0,支付成功 1,支付失败 2,待退款 3,已退款 4
	//基本工资+加班费+全勤奖+技术津贴+行政奖励+职务津贴+工龄奖金+绩效奖+其它补助等
    basesalary("baseSalary", "基本工资"), 
    floatsalary("floatSalary", "浮动"),
    overtimesalary("overtimesalary", "加班工资"),
    bonus("bonus", "奖金"),allowance("allowance", "补贴"),
    othersplus("othersplus", "其他加项"),
    othersdeduct("othersdeduct", "其他扣款"),
    jiaban("jiaban", "加班天数"),
    queqin("queqin", "缺勤天数"),
    totalPerson("totalPerson", "个人社保合计"),
    houseFundsPersonalNum("houseFundsPersonalNum", "个人住房公积金");
	
	
	
	
    private String value;
    private String desc;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private ColumnType(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}