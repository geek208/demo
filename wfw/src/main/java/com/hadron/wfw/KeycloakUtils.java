package com.hadron.wfw;

import javax.servlet.http.HttpServletRequest;

import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.representations.AccessToken;

public class KeycloakUtils {

	public static String getKeycloakSecurityContext(HttpServletRequest request) {
		// TODO Auto-generated method stub
		//KeycloakSecurityComponents session = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
		
		KeycloakSecurityContext sc= (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
		
		
		if(sc !=null){
			
			System.err.println("id token =" + sc.getIdTokenString() +" token==" +sc.getToken()+sc.getToken().getEmail());
			return sc.getToken().getPreferredUsername();
		}else{
			return null;
		}
		
	    //log.info("User Greeting for {} {}", principal.getName(), keycloakSecurityContext.getToken().getPreferredUsername());
		//return sc.getToken().getPreferredUsername();
	}
	
	public static AccessToken  getToken(HttpServletRequest request) {
		// TODO Auto-generated method stub
		//KeycloakSecurityComponents session = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
		
		KeycloakSecurityContext sc= (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
		
		if(sc == null){
			
			return null;
			
		}else{
			if(sc !=null){
				
				System.err.println("id token =" + sc.getIdTokenString() +" token==" +sc.getToken()+sc.getToken().getEmail());
				return sc.getToken();
			}else{
				return null;
			}
		}
	    //log.info("User Greeting for {} {}", principal.getName(), keycloakSecurityContext.getToken().getPreferredUsername());
		//return sc.getToken().getPreferredUsername();
	}

}
