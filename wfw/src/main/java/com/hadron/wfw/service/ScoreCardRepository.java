package com.hadron.wfw.service;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.hadron.wfw.model.ScoreCard;

/**
 * 
 * @author xuychao
 * @date 2022年3月15日
 * @classname EasyPayRepository.java
 * @email xuychao@163.com  git@github.com:geek208/wfw.git
 */


public interface ScoreCardRepository extends JpaRepository<ScoreCard, Long> {
    /**
     * Find by id user.
     *
     * @param id the id
     * @return the user
     */
	ScoreCard findById(long id);
	
	@Transactional
	@Modifying
	//@Query("update t_stock   set count = count-1 where id in ids",nativeQuery = true)
	
	@Query(value = "select count(*) from t_wfw_task  WHERE pid =?1  AND  current_id =?2 AND status =1",nativeQuery = true)
	int   findAmount(String  pid,String activityId);
	
	@Query(value = "UPDATE t_score_card SET amount =?2   WHERE order_sn =?1",nativeQuery = true)
	public void updateAmount(String orderSn,String amount);

}
