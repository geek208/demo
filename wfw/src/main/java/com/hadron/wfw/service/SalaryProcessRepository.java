package com.hadron.wfw.service;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hadron.wfw.model.SalaryItem;
import com.hadron.wfw.model.SalaryProcess;
import com.hadron.wfw.model.SalaryTemplate;


/**
 * 
 * @author xuychao
 * @date 2022年3月15日
 * @classname OrgRepository.java
 * @email xuychao@163.com  git@github.com:geek208/wfw.git
 */
public interface SalaryProcessRepository extends JpaRepository<SalaryProcess, Long> {
    /**
     * Find by id user.
     *
     * @param id the id
     * @return the user
     */
	SalaryProcess findById(int id);
	
	@Query(value = "select  * from t_salary_process a  WHERE a.template_id =?1",nativeQuery = true )
	SalaryProcess   findByTempId(String pId);

	//void save(WfwOrgUser au);
}
