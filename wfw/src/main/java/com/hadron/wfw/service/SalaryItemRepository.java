package com.hadron.wfw.service;


import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.hadron.wfw.model.SalaryItem;
import com.hadron.wfw.model.WfwActivity;
import com.hadron.wfw.model.WfwActivityUser;
import com.hadron.wfw.model.WfwOrg;
import com.hadron.wfw.model.WfwOrgUser;
import com.hadron.wfw.model.WfwUser;

/**
 * 
 * @author xuychao
 * @date 2022年3月15日
 * @classname OrgRepository.java
 * @email xuychao@163.com  git@github.com:geek208/wfw.git
 */
public interface SalaryItemRepository extends JpaRepository<SalaryItem, Long> {
    /**
     * Find by id user.
     *
     * @param id the id
     * @return the user
     */
	SalaryItem findById(int id);
	
	//找到处理人
	@Query(value = "select  * from t_salary_item a  WHERE a.p_id =?1",nativeQuery = true )
	List <SalaryItem>   findByPId(String pId);
	
	//找到处理人
	@Transactional
    @Modifying
    @Query(value = "delete from t_salary_item where p_id =?1",nativeQuery = true)
    void delAccount(int id);
//	@Query(value = "select  * from t_salary_item a  WHERE a.p_id =?1",nativeQuery = true )
//	List <SalaryItem>   findByPIdw(String pId);
	
	@Query(value = "select  * from t_salary_item a  WHERE a.reffield_column =?1 AND a.user_id =?2",nativeQuery = true )
	SalaryItem  findByUserId(String  cloumn,String userId);
	
	@Query(value = "select  * from t_salary_item a  WHERE a.temp_id =?1 AND a.p_id =?2 AND a.user_id =?3 AND a.rules_type=?4 ",nativeQuery = true )
	List <SalaryItem>    findByTempPIdUserId(String  tempId,String  pId,String userId,String rulesType);
	
	@Query(value = "select  * from t_salary_item a  WHERE a.temp_id =?1 AND a.p_id =?2 AND a.user_id =?3 ",nativeQuery = true )
	List <SalaryItem>    findByTempPIdUserId(String  tempId,String  pId,String userId);
	
	@Query(value = "select  * from t_salary_item a  WHERE a.reffield_column =?1 AND a.user_id =?2 AND  a.p_id =?3",nativeQuery = true )
	SalaryItem  findByColAndPId(String  colId,String userId,String pid);
	
	@Query(value = "select  * from t_salary_item a WHERE a.temp_id =?1  AND  (p_id is null or p_id='')",nativeQuery = true )
 	List <SalaryItem>  findByTempId(String  tempId);
	
	@Query(value = "select  * from t_salary_item a WHERE a.temp_id =?1 AND a.is_tax =?2  AND  (p_id is null or p_id='')",nativeQuery = true )
 	List <SalaryItem>  findTaxByTempId(String  tempId,String isTax);
	
	@Query(value = "select  * from t_salary_item a WHERE a.temp_id =?1 AND a.is_tax =?2  a.rules_type =?3  AND  (p_id is null or p_id='')",nativeQuery = true )
 	List <SalaryItem>  findTaxCalByTempId(String  tempId,String isTax,String cal);
}
