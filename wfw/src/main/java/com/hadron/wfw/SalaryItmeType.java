package com.hadron.wfw;


/**
 * 
 * @author xuychao
 * @date 2022年3月15日
 * @classname ActionType.java
 * @email xuychao@163.com  git@github.com:geek208/wfw.git
 */

public enum SalaryItmeType {
	
	//未支付 0,支付成功 1,支付失败 2,待退款 3,已退款 4

    HR("0", "人事"), ATTENDANCE("1", "考情项目"),PSOCIALSECURITY("2", "个人社保公积金"),CPYSOCIALSECURITY("3", "企业社保公积金"),DEDUCT("4", "专项附件扣除项目"),TAXDEDUCT("5", "个税扣缴项目");

    private String value;
    private String desc;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private SalaryItmeType(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}