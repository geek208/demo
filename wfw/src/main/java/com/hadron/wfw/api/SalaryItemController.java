package com.hadron.wfw.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hadron.app.PlatformCompanyRepository;
import com.hadron.app.SalaryAttendanceRepository;
import com.hadron.app.SalaryColumnCodeRepository;
import com.hadron.app.SalaryInsurancePlanRepository;
import com.hadron.app.SalaryInsuranceRepository;
import com.hadron.wfw.CalculationUtil;
import com.hadron.wfw.ColumnUtil;
import com.hadron.wfw.HttpAPIService;
import com.hadron.wfw.KeycloakUtils;
import com.hadron.wfw.RedisService;
import com.hadron.wfw.ResultData;
import com.hadron.wfw.cache.UserCache;
import com.hadron.wfw.model.PlatformCompany;
import com.hadron.wfw.model.SalaryAttendance;
import com.hadron.wfw.model.SalaryColumnCode;
import com.hadron.wfw.model.SalaryDataTable;
import com.hadron.wfw.model.SalaryInsurance;
import com.hadron.wfw.model.SalaryInsurancePlan;
import com.hadron.wfw.model.SalaryTemplate;
import com.hadron.wfw.model.SysUser;
import com.hadron.wfw.producer.Producer;
import com.hadron.wfw.service.SalaryItemRepository;
import com.hadron.wfw.service.SalaryTemplateRepository;
import com.hadron.wfw.service.UserRepository;

import antlr.collections.List;
import io.swagger.annotations.ApiOperation;

/**
 * User controller
 * <p/>
 * Created in 2018.11.16
 * <p/>
 *
 */
@Controller
@RestController
@RequestMapping("/salaryitem")
public class SalaryItemController {
	@Autowired
	UserCache userCache;

	static double RAITO = 0.01;

	@Autowired
	private HttpServletRequest request;
	@Autowired
	private PlatformCompanyRepository platformCompanyRepository;

	@Autowired
	private SalaryAttendanceRepository salaryAttendanceRepository;

	@Autowired
	private SalaryInsuranceRepository salaryInsuranceRepository;
	
	
	@Autowired
	private SalaryColumnCodeRepository salaryColumnCodeRepository;
	
	
	
	@Autowired
	SalaryItemRepository salaryItemRepository;

	@Autowired
	private SalaryInsurancePlanRepository salaryInsurancePlanRepository;
	
	@Autowired
	SalaryTemplateRepository salaryTemplateRepository;

	@Autowired
	UserRepository userRepository;

	// @Autowired
	// private ScoreCardRepository scoreCardRepository;

	@Autowired
	private HttpAPIService httpAPIService;
	@Autowired
	private Producer producer;
	// @Autowired
	// private KafkaTemplate kafkaTemplate;
	@Autowired
	private RedisService redisService;
	@Autowired
	private Environment env;

	private static Gson gson = new GsonBuilder().create();

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/createCompany")
	@ResponseBody
	// @RequestBody
	// http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1
	public ResultData createCompany(@RequestBody PlatformCompany company) {

		platformCompanyRepository.save(company);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(company);
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/createAttendance")
	@ResponseBody
	// @RequestBody
	// http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1
	public ResultData createAttendance(@RequestBody SalaryAttendance attendance) {

		SysUser u = userRepository.findById(Integer.parseInt(attendance.getUserId()));
		attendance.setCpyId(u.getCpyId());

		// 实际出勤 = 应出勤-所有请假
		float queqin = attendance.getShijia() + attendance.getBingjia() + attendance.getHunjia()
				+ attendance.getSangjia() + attendance.getKuanggong();

		attendance.setQueqin(queqin);
		attendance.setShijichuqin(attendance.getYingchuqin() - queqin);

		salaryAttendanceRepository.save(attendance);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(attendance);
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/createInsurance")
	@ResponseBody
	// @RequestBody
	// http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1
	public ResultData createInsurance(@RequestBody SalaryInsurance insurance) {
		SysUser u = userRepository.findById(Integer.parseInt(insurance.getUserId()));
		insurance.setCpyId(u.getCpyId());
		SalaryInsurancePlan plan = salaryInsurancePlanRepository.findById(Integer.parseInt(insurance.getCity()));
		
		insurance.setOldageCompanyNum(CalculationUtil.formatDouble2(plan.getOldageCompanyRatio() * RAITO * insurance.getBasefee()));
		insurance.setOldagePersonalNum(CalculationUtil.formatDouble2(plan.getOldagePersonalRatio() * RAITO * insurance.getBasefee()));

		insurance.setMedicalCompanyNum(CalculationUtil.formatDouble2(plan.getMedicalCompanyRatio() * RAITO * insurance.getBasefee()));
		insurance.setMedicalPersonalNum(CalculationUtil.formatDouble2(plan.getMedicalPersonalRatio() * RAITO * insurance.getBasefee()));

		insurance.setBirthCompanyNum(CalculationUtil.formatDouble2(plan.getBirthCompanyRatio() * RAITO * insurance.getBasefee()));
		insurance.setBirthPersonalNum(CalculationUtil.formatDouble2(plan.getBirthPersonalRatio() * RAITO * insurance.getBasefee()));

		insurance.setInjuryCompanyNum(
				CalculationUtil.formatDouble2(plan.getInjuryCompanyRatio() * RAITO * insurance.getBasefee()));
		insurance.setInjuryPersonalNum(
				CalculationUtil.formatDouble2(plan.getInjuryPersonalRatio() * RAITO * insurance.getBasefee()));

		insurance.setUnemploymentCompanyNum(CalculationUtil.formatDouble2(plan.getUnemploymentCompanyRatio() * RAITO * insurance.getBasefee()));
		insurance.setUnemploymentPersonalNum(CalculationUtil.formatDouble2(plan.getBirthPersonalRatio() * RAITO * insurance.getBasefee()));

		insurance.setHouseFundsCompanyNum(CalculationUtil.formatDouble2(plan.getHouseFundsCompanyRatio() * RAITO * insurance.getBasefee()));
		insurance.setHouseFundsPersonalNum(CalculationUtil.formatDouble2(plan.getHouseFundsPersonalRatio() * RAITO * insurance.getBasefee()));

		insurance.setTotalCompany(CalculationUtil.formatDouble2(
				insurance.getOldageCompanyNum() + insurance.getMedicalCompanyNum() + insurance.getInjuryCompanyNum()
						+ insurance.getBirthCompanyNum() + insurance.getUnemploymentCompanyNum()));
		insurance.setTotalPerson(CalculationUtil.formatDouble2(
				insurance.getOldagePersonalNum() + insurance.getMedicalPersonalNum() + insurance.getInjuryPersonalNum()
						+ insurance.getBirthPersonalNum() + insurance.getUnemploymentPersonalNum()));

		insurance.setTotal(CalculationUtil.formatDouble2(insurance.getTotalCompany() + insurance.getTotalPerson()
				+ insurance.getHouseFundsCompanyNum() + insurance.getHouseFundsPersonalNum()));
		
		
		Map<String,Object> map=new HashMap<>();
        map.put(ColumnUtil.getFieldName(SalaryInsurance::getOldagePersonalNum),insurance.getOldagePersonalNum());
        
        map.put(ColumnUtil.getFieldName(SalaryInsurance::getMedicalPersonalNum),insurance.getMedicalPersonalNum());
        
        map.put(ColumnUtil.getFieldName(SalaryInsurance::getInjuryPersonalNum),insurance.getInjuryPersonalNum());
        

        //获取map的key 和value
        //key 为key1
        String key1="aaa";
        //value 为 map.get(key1)
       System.err.println(map.get(ColumnUtil.getFieldName(SalaryInsurance::getOldagePersonalNum)));
       
       
		salaryInsuranceRepository.save(insurance);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(insurance);
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/createInsurancePlan")
	@ResponseBody
	// @RequestBody
	// http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1
	public ResultData createInsurancePlan(@RequestBody SalaryInsurancePlan insurance) {

		// SysUser u =
		// userRepository.findById(Integer.parseInt(insurance.getUserId()));
		// insurance.setCpyId(u.getCpyId());

		salaryInsurancePlanRepository.save(insurance);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(insurance);
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	// @RequestMapping("/joinCompany")
	@GetMapping("/joinCompany/{id}")
	// @SuppressWarnings("rawtypes")
	@ResponseBody
	// @RequestBody
	// http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1
	public ResultData joinCompany(@PathVariable String id) {
		// platformCompanyRepository.save(company);
		String username = KeycloakUtils.getToken(request).getPreferredUsername();
		// 加入公司
		SysUser u = userRepository.findUserByName(username);

		PlatformCompany company = platformCompanyRepository.findById(Integer.parseInt(id));

		u.setCpyId(id);
		u.setCompany(company.getCompanyName());
		userRepository.save(u);

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(id);
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getCompanyList")
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getCompanyList() throws Exception {

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(platformCompanyRepository.findAll());
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getAttendaceList")
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getAttendaceList() throws Exception {

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryAttendanceRepository.findAll());
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getinsuranceList")
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getinsuranceList() throws Exception {

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryInsuranceRepository.findAll());
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getinsuranceplanList")
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getinsuranceplanList() throws Exception {

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryInsurancePlanRepository.findAll());
		return data;
	}
	
	
	
	
	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getColumnList/{id}")
	
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	
	public ResultData getColumnList(@PathVariable String  id) throws Exception {

		//ColumnUtil.getFieldName(TestUserDemo::getCompanySimpleName))
	    Map<String, String> map=new HashMap<>();
//		//如果去人事数据
//		if(SalaryItmeType.HR.getValue().equalsIgnoreCase(id)){
//			SysUser  u = new SysUser();
//			//u.getAddress()
//			//u.getAllowance();
//			//u.getBankCardNum()
//			map.put(ColumnUtil.getFieldName(SysUser::getAddress),"地址");
//			map.put(ColumnUtil.getFieldName(SysUser::getAllowance),"津贴");
//			map.put(ColumnUtil.getFieldName(SysUser::getBankCardNum),"银行卡");
//		}
		
		//gson.
		
//		String json = salaryInsuranceRepository.findById(1582).getJson();
//		
//		Map maps2 = JSON.parseObject(json,Map.class);
//        System.out.println("使用JSON类，指定解析类，解析json字符串：");
//        for (Object key:maps2.keySet()) {
//            System.out.println("key:" + key + "->value:" + maps2.get(key));
//            SalaryColumnCode column  =new  SalaryColumnCode();
//            
//            //column.setCode(key.toString());
//            //column.setCnName("工资");
//            //column.setType("1");
//            
//            salaryColumnCodeRepository.save(column);
//        }
        
        

		ResultData data = new ResultData();
		
		
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		//data.setData(map);
		data.setData(salaryColumnCodeRepository.findUserByType(id));
		return data;
	}
	
	
	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getColumnList2/{id}")
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	
	public ResultData getColumnList2(@PathVariable String  id) throws Exception {
		
		//salaryInsuranceRepository.findAll()		 
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryInsuranceRepository.findById(1581).getJson());
		return data;
	}
	
	
	
	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getColumnList3/{id}")
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	
	public ResultData getColumnList3(@PathVariable String  id) throws Exception {
		
		//salaryInsuranceRepository.findAll()		 
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryColumnCodeRepository.findAll());
		return data;
	}
	
	
	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getRowList/{id}")
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	
	public ResultData getRowList(@PathVariable String  id) throws Exception {

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryItemRepository.findAll());
		return data;
	}
	
	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getTableList/{id}")
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	
	public ResultData getTableList(@PathVariable String  id) throws Exception {
		
		ArrayList<SalaryDataTable> list  = new ArrayList();
		SalaryDataTable table  = new SalaryDataTable();

		table.setLable("考勤");
		table.setValue("1");
		
		list.add(table);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryItemRepository.findAll());
		return data;
	}
	
	
	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/createColumn")
	@ResponseBody
	public ResultData createColumn(@RequestBody SalaryColumnCode item) {
		
//		String username = KeycloakUtils.getToken(request).getPreferredUsername();
//    	//加入公司
//     	SysUser u =userRepository.findUserByName(username);
//     	
//     	item.setCpyId(u.getCpyId());
//     	item.setUserId(String.valueOf(u.getId()));
     	
     	
     	salaryColumnCodeRepository.save(item);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(item);
		return data;
	}
	
	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/updateColumn")
	@ResponseBody
	public ResultData updateColumn(@RequestBody SalaryColumnCode item) {
		
//		String username = KeycloakUtils.getToken(request).getPreferredUsername();
//    	//加入公司
//     	SysUser u =userRepository.findUserByName(username);
//     	
//     	item.setCpyId(u.getCpyId());
//     	item.setUserId(String.valueOf(u.getId()));
     	
     	
     	salaryColumnCodeRepository.save(item);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(item);
		return data;
	}
	
	
	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@GetMapping("/getColumn/{id}")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getColumn(@PathVariable String id) throws Exception {
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryColumnCodeRepository.findById(Integer.parseInt(id)));

		return data;
	}
	
	/**
	 * 1,  先   fill  引用字段
	 * 2,  计算统计字段
	 
	 * @param id
	 * @return
	 */
	
	@GetMapping("/startProcess/{id}")
	@ResponseBody
	public ResultData startProcess(@PathVariable String id) {
		
		//查找企业下所有人员
		SalaryTemplate template = salaryTemplateRepository.findById(Integer.parseInt(id));
		List  users = (List) userRepository.findUserByCpy(template.getCpyId());
		

		
		// 查找模板所有薪资项
	    List wfwFormFields = (List) salaryItemRepository.findByTempId(id);
	    
	    

	    ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryColumnCodeRepository.findById(Integer.parseInt(id)));

		return data;
	}

}