package com.hadron.wfw.api;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hadron.app.PlatformApp;
import com.hadron.app.PlatformAppRepository;
import com.hadron.wfw.HttpAPIService;
import com.hadron.wfw.RedisService;
import com.hadron.wfw.ResultData;
import com.hadron.wfw.cache.UserCache;
import com.hadron.wfw.producer.Producer;

import io.swagger.annotations.ApiOperation;

/**
 * User controller
 * <p/>
 * Created in 2018.11.16
 * <p/>
 *
 */
@Controller
@RequestMapping("/app")
public class PlatformAppController {

    /**
     * The User service.
     */
  
    
    @Autowired
	UserCache userCache;
    
    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    private PlatformAppRepository platformAppRepository;

    
    @Autowired
    private HttpAPIService httpAPIService;
    @Autowired
    private Producer producer;
//    @Autowired
//    private KafkaTemplate kafkaTemplate;
	@Autowired
	private RedisService redisService;
	@Autowired
	private Environment env;
	
    private static Gson gson = new GsonBuilder().create();
    /**
     * Add string.
     *
     * @param user the user
     * @return the string
     */
    @RequestMapping("/createApp")
    @ResponseBody
    //@RequestBody http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1
    public ResultData createApp(@RequestBody PlatformApp app) {
    	
    	platformAppRepository.save(app);
        ResultData data =new ResultData();
        data.setCode(200);
        data.setSuccess(true);
        data.setMessage("成功");
        data.setData(app);
        return data;
    }
    
    
    /**
     * Add string.
     *
     * @param user the user
     * @return the string
     */
    @RequestMapping("/updateApp")
    @ResponseBody
    //@RequestBody http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1
    public ResultData updateApp(@RequestBody PlatformApp app) {
    	
    	platformAppRepository.save(app);
        ResultData data =new ResultData();
        data.setCode(200);
        data.setSuccess(true);
        data.setMessage("成功");
        data.setData(app);
        return data;
    }
    
    
    
    


	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getAppList")
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getAppList() throws Exception {

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(platformAppRepository.findAll());
		return data;
	}
	
	
	@GetMapping("/getApp/{id}")
	//@GetMapping("/getSalaryItem/{id}")
	// @SuppressWarnings("rawtypes")
	@ResponseBody
	// @RequestBody
	// http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1
	public ResultData getApp(@PathVariable String id) {
		// platformCompanyRepository.save(company);
		PlatformApp app =platformAppRepository.findById(Integer.parseInt(id));

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(app);
		return data;
	}
}