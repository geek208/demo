package com.hadron.wfw.api;


import java.io.IOException;
import java.util.Date;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import com.hadron.wfw.KeycloakUtils;
import com.hadron.wfw.Utils;
import com.hadron.wfw.cache.UserCache;
import com.hadron.wfw.model.SysUser;
import com.hadron.wfw.model.UserVO;
import com.hadron.wfw.service.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AuthInterceptor implements HandlerInterceptor {
    private static final String COOKIE_NAME = "auth2222";
    @Autowired
    UserCache userCache;
    @Autowired
    UserRepository userRepository;
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
         boolean result= check(request, response, handler);
        
         String methodName="";
         if (handler instanceof HandlerMethod){
             HandlerMethod handler1 = (HandlerMethod) handler;
             methodName=  handler1.getMethod().getName();
         }
         
         log.info(request.getRequestURI()+",methodName:"+methodName+",auth result:"+ result);
         System.err.println(request.getRequestURI()+",methodName:"+methodName+",auth result:"+ result);
        return result;
    }

    private boolean check(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
    	
    	//response.sendRedirect("http://10.0.0.122:8180");
        
    	if (handler ==null) return true;
//        PreAuth preAuth = verifyAuth(handler);
//        //无需要认证
//        if (preAuth ==null) return  true;
        String username=null;
		try {
			username = KeycloakUtils.getToken(request).getPreferredUsername();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//response.setStatus(302);
        	System.err.println("異常重定向到登錄頁面");
			//response.setHeader("location", "http://10.0.0.122:8180");
        	//KeycloakUtils.getKeycloakSecurityContext(request)
			e.printStackTrace();
			try {
				request.logout();
			} catch (ServletException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			return false;
		}

		UserVO userVO = this.userCache.getCache(username);
		
        //如果沒
        if(username == null ){
        	//response.setStatus(302);
        	System.err.println("重定向到登錄頁面  http://10.0.0.122:8180");
        	System.err.println("重定向到登錄頁面==========="+request.getRequestURI());
			//response.setHeader("location", "http://10.0.0.122:8180");
           // response.sendRedirect(request.getRequestURI());
            
//			try {
//				//request.logout();
//			} catch (ServletException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			return false;
        }
        
        //重新登录
        if (username !=null && userVO ==null) {
        	
			String cKey = Utils.getCharAndNum(10);
			Cookie cookie = new Cookie(COOKIE_NAME, cKey);
			// cookie.setDomain(domain);
			// 12个小时
			cookie.setMaxAge(60 * 60 * 12);
			cookie.setPath("/");
			// xss
			// cookie.setHttpOnly(true);
			// response.addHeader("Access-Control-Allow-Credentials", "true");
			response.addCookie(cookie);
			
			SysUser loginUser = userRepository.findUserByName(username);
			
			if(loginUser == null){
				SysUser u =new SysUser();
				u.setUserName(KeycloakUtils.getToken(request).getPreferredUsername());
				u.setEmail(KeycloakUtils.getToken(request).getEmail());
				
				Set<String> roles = KeycloakUtils.getToken(request).getRealmAccess().getRoles();
				
				StringBuffer sb = new  StringBuffer();
				for (String str : roles) {  
				      System.out.println(str);  
				      sb.append(str+",");
				} 

				//u.setRole(;
				u.setRemark(KeycloakUtils.getToken(request).getProfile());
				u.setCreateTime(new Date());
				u.setPhoneNum(KeycloakUtils.getToken(request).getPhoneNumber());
				u.setRole(sb.toString());
				userRepository.save(u);
			}

			//SysUser loginUser = userRepository.findUserByName(username);
//			UserVO userVO2 = loginUser.toVO(UserVO.class);
//			userCache.setCache(username, userVO2);

			// Result<Object> result =
			// Result.builder().code(403).message("请重新登录").build();
			// response.setHeader("content-type","application/json;charset=UTF-8");
			// response.getOutputStream().write(JSON.toJSONBytes(result));
			// response.flushBuffer();
            return true;
        }

//        String role = userCache.getRole();
//        //超级管理员
//        if (role.equals("admin")) return  true;
//
//        String[] roles = preAuth.value();
//        for (String need: roles){
//            if (role.equals(need)){
//                return true;
//            }
//
//        }
        //其他
//        Result<Object> result = Result.builder().code(403).message("无权限").build();
//        response.setHeader("content-type","application/json;charset=UTF-8");
//        response.getOutputStream().write(JSON.toJSONBytes(result));
//        response.flushBuffer();
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
       /* if (ex != null) {
            log.error("catch exception:{}", ex);
            String message = ex.getMessage();
            Result<Object> build = Result.builder().code(500).message(message).build();
            //response.sendError(500, JSON.toJSONString(build));
            response.getWriter().write(JSON.toJSONString(build));
            return;
        }*/
    }
    private PreAuth verifyAuth(Object handler) {
        PreAuth permit= null;
        if (handler instanceof HandlerMethod){
            permit = ((HandlerMethod) handler).getMethod().getAnnotation(PreAuth.class);
            if(permit == null){
                permit = ((HandlerMethod) handler).getBeanType().getAnnotation(PreAuth.class);
            }
        }
        return permit;
    }

   public String getAuthCookie(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
         if (cookies==null) return null;
        for (Cookie cookie:cookies){
            if (cookie.getName().equals(COOKIE_NAME)){
                return  cookie.getValue();
            }
        }
        return  null;
    }
}
