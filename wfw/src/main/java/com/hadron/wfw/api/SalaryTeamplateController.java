package com.hadron.wfw.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hadron.app.SalaryAttendanceRepository;
import com.hadron.app.SalaryInsuranceRepository;
import com.hadron.app.SalaryPersonalTaxplanRepository;
import com.hadron.wfw.ActionType;
import com.hadron.wfw.CalculationUtil;
import com.hadron.wfw.ColumnType;
import com.hadron.wfw.GetPayNo;
import com.hadron.wfw.HttpAPIService;
import com.hadron.wfw.IntervalUtil;
import com.hadron.wfw.JsonUtil;
import com.hadron.wfw.KeycloakUtils;
import com.hadron.wfw.PayStatus;
import com.hadron.wfw.RedisService;
import com.hadron.wfw.ResultData;
import com.hadron.wfw.SalaryPhase;
import com.hadron.wfw.SalaryStatisticsType;
import com.hadron.wfw.SalaryType;
import com.hadron.wfw.SourceType;
import com.hadron.wfw.model.EasyPay;
import com.hadron.wfw.model.SalaryColumnCode;
import com.hadron.wfw.model.SalaryGroup;
import com.hadron.wfw.model.SalaryInsurance;
import com.hadron.wfw.model.SalaryItem;
import com.hadron.wfw.model.SalaryPersonalTaxPlan;
import com.hadron.wfw.model.SalaryProcess;
import com.hadron.wfw.model.SalaryProcessVO;
import com.hadron.wfw.model.SalaryTemplate;
import com.hadron.wfw.model.SalaryUser;
import com.hadron.wfw.model.SysUser;
import com.hadron.wfw.model.WfwActivity;
import com.hadron.wfw.model.WfwActivityRules;
import com.hadron.wfw.model.WfwFlow;
import com.hadron.wfw.model.UserField;
import com.hadron.wfw.model.UserSalaryVO;
import com.hadron.wfw.model.WfwProcess;
import com.hadron.wfw.model.WfwUser;
import com.hadron.wfw.producer.Producer;
import com.hadron.wfw.service.EasyPayService;
import com.hadron.wfw.service.SalaryGroupRepository;
import com.hadron.wfw.service.SalaryItemRepository;
import com.hadron.wfw.service.SalaryProcessRepository;
import com.hadron.wfw.service.SalaryTemplateRepository;
import com.hadron.wfw.service.UserFieldRepository;
import com.hadron.wfw.service.UserRepository;
import com.hadron.wfw.service.WfwUserRepository;
import com.ql.util.express.DefaultContext;
import com.ql.util.express.ExpressRunner;

import io.swagger.annotations.ApiOperation;

/**
 * User controller
 * <p/>
 * Created in 2018.11.16
 * <p/>
 *
 */
@Controller
@RequestMapping("/salary")
public class SalaryTeamplateController {

	/**
	 * The User service.
	 */
	@Autowired
	EasyPayService payService;
	@Autowired
	private HttpAPIService httpAPIService;
	@Autowired
	private Producer producer;

	@Autowired
	private HttpServletRequest request;
	/*
	 * @Autowired private KafkaTemplate kafkaTemplate;
	 */
	@Autowired
	private RedisService redisService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	SalaryProcessRepository salaryProcessRepository;

	@Autowired
	SalaryItemRepository salaryItemRepository;

	@Autowired
	SalaryTemplateRepository salaryTemplateRepository;

	@Autowired
	SalaryGroupRepository salaryGroupRepository;

	@Autowired
	private UserFieldRepository userFieldRepository;

	@Autowired
	private SalaryAttendanceRepository salaryAttendanceRepository;

	@Autowired
	private SalaryInsuranceRepository salaryInsuranceRepository;

	// SalaryPersonalTaxplanRepository

	@Autowired
	private SalaryPersonalTaxplanRepository salaryPersonalTaxplanRepository;

	@Autowired
	private WfwUserRepository wfwUserRepository;

	@Autowired
	private Environment env;

	private static Gson gson = new GsonBuilder().create();
	
	Calendar cal = Calendar.getInstance();


	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/createTemplate")
	@ResponseBody
	public ResultData createTemplate(@RequestBody SalaryTemplate template) {

		String username = KeycloakUtils.getToken(request).getPreferredUsername();
		// 加入公司
		SysUser u = userRepository.findUserByName(username);

		template.setCpyId(u.getCpyId());
		template.setUserId(String.valueOf(u.getId()));
		template.setCompany(u.getCompany());

		salaryTemplateRepository.save(template);
		ResultData data = new ResultData();

		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(template);
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/createItem")
	@ResponseBody
	public ResultData createItem(@RequestBody SalaryItem item) {

		String username = KeycloakUtils.getToken(request).getPreferredUsername();
		// 加入公司
		SysUser u = userRepository.findUserByName(username);

		item.setCpyId(u.getCpyId());
		item.setUserId(String.valueOf(u.getId()));

		salaryItemRepository.save(item);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(item);
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/createSalrayUser")
	@ResponseBody
	public ResultData createSalrayUser(SalaryUser pay) {
		salaryTemplateRepository.save(pay);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(pay);
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/createSalrayGroup")
	@ResponseBody
	public ResultData createSalrayGroup(SalaryGroup pay) {
		salaryGroupRepository.save(pay);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(pay);
		return data;
	}

	/**
	 * 1,按阶段计算 ，每个计算阶段可以反复操作 2，算法 a，先找模板绑定算薪人员 b,找本次计算进程选中算薪人员 c，移出排除算薪人员
	 * d,移除未定薪人员，移除已离职---修正，模板绑定未定薪或者已离职人员， 3，得到本次应算薪人员 4,按待算人员清单遍历，启动异步线程计算
	 * 
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/start")
	@ResponseBody
	public ResultData start(String pid, String stage) {

		SalaryProcess process = salaryProcessRepository.findById(Integer.parseInt(pid));

		// 更新计算阶段
		process.setStage(stage);
		// 更新计算进程
		salaryProcessRepository.save(process);

		// 获取算薪人员
		List<Map<String, String>> obj = wfwUserRepository.findByTemplateId(String.valueOf(process.getTemplateId()));
		String irs = JSON.toJSONString(obj);
		List<WfwUser> users = JSON.parseArray(irs, WfwUser.class);
		System.err.println("找到需要算薪的人员=" + users.size());
		// 遍历待算薪人员
		for (WfwUser wfwUser : users) {
			// 计算税前
			if (process.getStage().equalsIgnoreCase(SalaryPhase.BRFORETAX.getValue())) {
				// 找出待算薪统计项
				List<SalaryItem> wfwFormFields = salaryItemRepository.findTaxCalByTempId(process.getTemplateId(),
						SalaryPhase.BRFORETAX.getValue(), SalaryType.CALCULATE.getValue());
				// 循环计算
				for (SalaryItem salaryItem : wfwFormFields) {
					this.cal(salaryItem, String.valueOf(wfwUser.getId()));
				}
				// 计算税后
			} else {
				// 找出待算薪统计项
				List<SalaryItem> wfwFormFields = salaryItemRepository.findTaxCalByTempId(process.getTemplateId(),
						SalaryPhase.AFTERTAX.getValue(), SalaryType.CALCULATE.getValue());
				// 循环计算
				for (SalaryItem salaryItem : wfwFormFields) {
					this.cal(salaryItem, String.valueOf(wfwUser.getId()));
				}
			}
		}
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(process);
		return data;
	}

	// 计算方式
	public void cal(SalaryItem object, String userId) {
		// 只算 自定义公式
		if (SalaryType.CALCULATE.getValue().equals(object.getRulesType())) {
			System.err.println("数据来源       类型=" + object.getRulesType() + "  自定义计算公式=[" + object.getRules() + "]");
			// 自定义公式
			// 原公式 a=(b-c)/(d-c)
			// &a&=&(&b&-&c&)&/&(&d&-&c&)&
			String val = object.getRules();
			// 分解计算公式 ，得到变量名称
			String[] aa1 = val.split("&");
			// 去掉转义符号
			String newstr = val.replace("&", "");
			// 构造计算器
			ExpressRunner runner = new ExpressRunner(false, false);
			DefaultContext<String, Object> context = new DefaultContext<String, Object>();

			// 拼接公式
			String eq = "a=" + newstr;

			// 循环动态赋值
			for (String string : aa1) {
				// System.out.println(string+"===="+CalculationUtil.isNumericZidai(string));

				// 检查是否是字符串，此处能不用id，需要用name，id无法注入规则引擎
				if (string != null && string.length() > 0 && CalculationUtil.check(string)) {
					// 获取基本薪资数据
					UserField userfield = userFieldRepository.findByNameUserId(string, userId);
					context.put(string, new BigDecimal(userfield.getFieldvalue()));
					// context.put("c", new
					// BigDecimal("0.15384615384615385"));
					// context.put("d", new BigDecimal("1"));
				}
			}
			Object r = null;
			try {
				r = runner.execute(eq, context, null, false, false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 设置计算结果
			System.err.println("设置计算结果      自定义计算公式=" + r.toString());
			object.setFieldValue(r.toString());

			// 保存计算结果
			salaryItemRepository.save(object);
		}

	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@GetMapping("/getTemplateList/{id}")
	@ResponseBody
	public ResultData getTemplateList(@PathVariable String id) throws Exception {

		List<SalaryTemplate> flows = salaryTemplateRepository.findAll();

		// WfwFormV formV =new WfwFormV();
		// formV.setFormfield(formField);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(flows);
		return data;

	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@GetMapping("/getSalaryUserList/{id}")
	@ResponseBody
	public ResultData getSalaryUserList(@PathVariable String id) throws Exception {

		List<SysUser> flows = userRepository.findAll();

		// WfwFormV formV =new WfwFormV();
		// formV.setFormfield(formField);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(flows);
		return data;

	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@GetMapping("/getSalaryItemList/{id}")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getSalaryItemList(@PathVariable String id) throws Exception {

		List<SalaryItem> list = salaryItemRepository.findByTempId(id);

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(list);

		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@GetMapping("/getSalaryItemBySource/{id}")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getSalaryItemBySource(@PathVariable String id) throws Exception {

		// 获取基本薪资数据
		List<UserField> userfields = userFieldRepository.findAll();

		// String [][] =new {{22,22}}
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(userfields);

		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@GetMapping("/getSalaryColumnList/{id}")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getSalaryColumnList(@PathVariable String id) throws Exception {

		List<UserField> fields = userFieldRepository.findByGroupId(id);

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(fields);

		return data;
	}

	@GetMapping("/getSalaryItem/{id}")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getSalaryItem(@PathVariable String id) throws Exception {

		SalaryItem salaryItem = salaryItemRepository.findById(Integer.parseInt(id));

		// List<UserField> fields = userFieldRepository.findByGroupId("0");
		//
		// StringBuffer sb = new StringBuffer();
		//
		// for (UserField userField : fields) {
		// sb.append(userField.getName() + ";");
		// }
		//
		// salaryItem.setReffieldColumn(sb.toString());

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryItem);
		return data;
	}

	// @GetMapping("/listSalaryProcess/{id}")
	@RequestMapping("/listSalaryProcess")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData listSalaryProcess() throws Exception {

		List<SalaryProcess> list = salaryProcessRepository.findAll();
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(list);
		return data;
	}

	// @RequestMapping("/listusersalary")
	@GetMapping("/listusersalary/{id}")
	@ResponseBody
	public ResultData listusersalary(@PathVariable String id) throws Exception {
		// 人员薪资
		List<UserSalaryVO> userSalaryVO = new ArrayList();
		//
		SalaryProcess process = salaryProcessRepository.findById(Integer.parseInt(id));
		// 获取算薪人员
		List<Map<String, String>> obj = wfwUserRepository.findByTemplateId(String.valueOf(process.getTemplateId()));
		String irs = JSON.toJSONString(obj);
		List<WfwUser> users = JSON.parseArray(irs, WfwUser.class);

		System.err.println("找到需要算薪的人员=" + users.size());
		// 查找模板所有薪资项
		List<SalaryItem> wfwFormFields = (List<SalaryItem>) salaryItemRepository.findByPId(id);

		UserSalaryVO userSalary = new UserSalaryVO();
		for (WfwUser user : users) {

			List<SalaryItem> salaryFields = new ArrayList();
			for (SalaryItem salaryItem : wfwFormFields) {
				if (salaryItem.getUserId().equalsIgnoreCase(String.valueOf(user.getId()))) {
					// 人员薪资
					salaryFields.add(salaryItem);
				} else {
					continue;
				}
			}

			userSalary.setId(Integer.parseInt(String.valueOf(user.getId())));
			userSalary.setUsername(user.getUserName());
			// 给人绑定薪资项
			userSalary.setSalaryItem(salaryFields);
			userSalaryVO.add(userSalary);
		}

		// List<SalaryProcess> list = salaryProcessRepository.findAll();
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(userSalaryVO);
		return data;
	}

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * 
	 *            1，可以按人异步计算 2，
	 * 
	 * 
	 * 
	 * 
	 *            应发工资 没有扣除你应缴应扣的各种项目；对应的应该有实发工资，就是扣除了你缴纳项后的实际领到手的薪资。
	 *            福利是一个很含糊的概念，比如年终红包、节假日的补助等，也是可以分开发放的，薪资管理制度上应该有规定，
	 *            是薪资明细单上应该明确显示的。有的话也是包含在应发工资里面
	 * 
	 *            实发工资=应发工资-应由劳动者个人承担的各项社会保险费用-住院 公积金 —应缴个人所得税
	 *            应发工资=基本工资+加班费+全勤奖+技术津贴+行政奖励+职务津贴+工龄奖金+绩效奖+其它补助等.
	 *            实发工资=应发工资-缺勤-行政处罚-养老保险-代缴税款等.
	 * 
	 * @return the string
	 */
	// @RequestMapping("/startProcess")

	@GetMapping("/startProcess/{id}")
	@ResponseBody
	public ResultData startProcess(@PathVariable String id) {

		// 人员薪资
		List<UserSalaryVO> userSalaryVO = new ArrayList();
		SalaryProcessVO salaryProcessVO = null;

		SalaryTemplate template = salaryTemplateRepository.findById(Integer.parseInt(id));

		SalaryProcess oldprocess = salaryProcessRepository.findByTempId(id);

		if (oldprocess == null) {
			// 生成进程
			SalaryProcess process = new SalaryProcess();
			// process.setTempalteName(template.getTempalteName());
			process.setTemplateId(String.valueOf(template.getId()));

			// 设置处理阶段
			process.setStage(SalaryPhase.BRFORETAX.getValue());
			process.setCpyId(template.getCpyId());
			
			
			process.setCircle(template.getCircle());

			process.setMonth(String.valueOf(cal.get(Calendar.MONTH) + 1));

			// 审批情况
			process.setApprove(ActionType.AGREE.getValue());
			// 默认不锁定
			process.setIslock("0");
			process.setPName(template.getTName());
			// 操作人员id
			process.setUserId("99999");

			// 获取算薪人员
			// List<Map<String, String>> obj =
			// wfwUserRepository.findByTemplateId(String.valueOf(template.getId()));
			// String irs = JSON.toJSONString(obj);
			//
			// List<WfwUser> users = JSON.parseArray(irs, WfwUser.class);

			List<SysUser> users = (List) userRepository.findUserByCpy(template.getCpyId());

			System.err.println("找到需要算薪的人员=" + users.size());

			// 待算薪人数
			process.setCount(users.size());

			// 调薪人数
			process.setAdjust(6);

			// 入职人数
			process.setEntry(5);
			process.setQuit(3);
			process.setDetermine(2);
			process.setPromotion(8);

			// 启动计算，
			salaryProcessRepository.save(process);

			// 查找模板所有薪资项
			List<SalaryItem> wfwFormFields = salaryItemRepository.findByTempId(id);

			// 待算薪人和薪资项都不能空
			if (users != null && users.size() > 0 && wfwFormFields != null) {
				// 遍历待算薪人员
				for (SysUser user : users) {
					System.err.println("遍历薪资模板，每个工资项按    每个人 计算生成一份=" + wfwFormFields.size() + "第[" + user.getId()
							+ "]人 +  名字" + user.getUserName());
					// 生成进程的薪资项快照,每个进程生成一份
					List<SalaryItem> newwfwFormFields = new ArrayList();

					// 统计输入源数据
					Map sourcehash = new Hashtable<>();

					UserSalaryVO userSalary = new UserSalaryVO();
					// 遍历薪资模板，每个工资项按 每个人 计算生成一份
					for (SalaryItem salaryitem : wfwFormFields) {
						// 薪资项快照
						SalaryItem snapshot = new SalaryItem();

						snapshot.setFieldName(salaryitem.getFieldName());
						snapshot.setFieldCode(salaryitem.getFieldCode());
						snapshot.setFieldType(salaryitem.getFieldType());
						snapshot.setGroupId(salaryitem.getGroupId());
						snapshot.setRules(salaryitem.getRules());
						snapshot.setRulesType(salaryitem.getRulesType());

						// 引用源
						snapshot.setReffieldColumn(salaryitem.getReffieldColumn());
						snapshot.setReffieldTable(salaryitem.getReffieldTable());

						// 绑定本进程,计算批次
						snapshot.setPId(String.valueOf(process.getId()));
						
						
						snapshot.setCpyId(template.getCpyId());
						snapshot.setTempId(process.getTemplateId());
						// field.setActivityId(object.getActivityId());
						// 从模板的哪个薪资字段生成
						snapshot.setParentId(String.valueOf(salaryitem.getId()));
						// 每个人一份
						snapshot.setUserId(String.valueOf(user.getId()));
						// 数据源映射
						if (SalaryType.QUOTE.getValue().equals(salaryitem.getRulesType())) {
							System.err.print("数据来源      类型=[" + salaryitem.getRulesType() + "]");
							// String rule = object.getRules();
							// 如果是薪资档案
							if (SourceType.BASE.getValue().equals(salaryitem.getReffieldTable())) {

								snapshot.setFieldValue(getSalaryValue(salaryitem, user.getJson()));

								// 考勤
							} else if (SourceType.ATTENDANCE.getValue().equals(salaryitem.getReffieldTable())) {
								System.err.println("数据来源  考勤[" + salaryitem.getReffieldTable() + "]");

								// SalaryInsurance ss
								// =salaryInsuranceRepository.findinsurance(String.valueOf(user.getId()));

								snapshot.setFieldValue(getSalaryValue(salaryitem, salaryAttendanceRepository
										.findSalaryAttendance(String.valueOf(user.getId())).getJson()));
								// 社保
							} else if (SourceType.insurance.getValue().equals(salaryitem.getReffieldTable())) {
								// System.err.println("数据来源 社保【" +
								// salaryitem.getReffieldTable() + "]");

								SalaryInsurance salaryInsurance = salaryInsuranceRepository
										.findinsurance(String.valueOf(user.getId()));

								snapshot.setFieldValue(getSalaryValue(salaryitem, salaryInsurance.getJson()));
								// snapshot.setFieldValue(getSalaryValue(salaryitem,
								// user.getJson()));
								// 个税
							} else if (SourceType.TAX.getValue().equals(salaryitem.getReffieldTable())) {
								System.err.println("数据来源  社保[" + salaryitem.getReffieldTable() + "]");

								// 获取基本薪资数据，需按人员获取
								// UserField userfield =
								// userFieldRepository.findByNameUserId(salaryitem.getReffieldColumn(),
								// String.valueOf(user.getId()));
								//
								// if (userfield != null) {
								// System.err.println("数据来源 userfield[" +
								// salaryitem.getReffieldColumn() + " 值["
								// + userfield.getFieldvalue() + "]");
								//
								// snapshot.setFieldValue(userfield.getFieldvalue());
								// // field.setFieldName(userfield.getCnName());
								// // System.err.println("数据来源 专项扣除【" +
								// // object.getReffieldTable()+"]");
								// //
								// salaryItem.setReffieldTable(object.getReffieldTable());
								// }
							}
							// 如果是计算公式
						} else if (SalaryType.CALCULATE.getValue().equals(salaryitem.getRulesType())) {

						} else {

							System.err.println("数据导入");
						}
						// 保存快照
						salaryItemRepository.save(snapshot);
						// 添加薪资项
						newwfwFormFields.add(snapshot);

						// 只加数字列
						if (snapshot != null && snapshot.getFieldType().equalsIgnoreCase("1")) {
							if (snapshot.getReffieldColumn() != null) {
								sourcehash.put(snapshot.getReffieldColumn(), snapshot.getFieldValue());
							}
						}

						System.err.println("用户名" + snapshot.getUserId() + "   注入之后  :::::字段名称="
								+ snapshot.getFieldName() + "  引用名字[" + snapshot.getReffieldColumn() + "]  值["
								+ snapshot.getFieldValue() + "]");
						System.err.println("json==" + JsonUtil.getJsonString(snapshot));

					}

					// 引用结束
					List<SalaryItem> list = salaryItemRepository.findByTempPIdUserId(process.getTemplateId(),
							String.valueOf(process.getId()), String.valueOf(user.getId()),
							SalaryType.CALCULATE.getValue());

					// 计算税前
					this.cal2(sourcehash, list);

					// 计算税后

					userSalary.setId(Integer.parseInt(String.valueOf(user.getId())));
					userSalary.setUsername(user.getUserName());

					// 给人绑定薪资项
					userSalary.setSalaryItem(newwfwFormFields);

					// 把工资项存到人员
					user.setSalary(JSON.toJSONString(sourcehash));
					userRepository.save(user);

					// 人员薪资
					userSalaryVO.add(userSalary);
				}

			}
			//

			salaryProcessVO = process.toVO(SalaryProcessVO.class);
			salaryProcessVO.setUserSalaryVO(userSalaryVO);

		} else {
			// 查找已有用戶酸心數據
			List<SysUser> users = (List) userRepository.findUserByCpy(template.getCpyId());

			for (SysUser sysUser : users) {
				System.err.println("模板  ========  [" + oldprocess.getTemplateId() + "   进程==[" + oldprocess.getId()
						+ "]" + "用户===[" + sysUser.getId() + "]");
				List<SalaryItem> list = salaryItemRepository.findByTempPIdUserId(oldprocess.getTemplateId(),
						String.valueOf(oldprocess.getId()), String.valueOf(sysUser.getId()));

				UserSalaryVO userSalary = new UserSalaryVO();
				userSalary.setId(sysUser.getId());
				userSalary.setUsername(sysUser.getUserName());
				// 给人绑定薪资项
				userSalary.setSalaryItem(list);

				userSalaryVO.add(userSalary);

				// List<SalaryItem> newwfwFormFields = new ArrayList();
			}

			salaryProcessVO = oldprocess.toVO(SalaryProcessVO.class);
			salaryProcessVO.setUserSalaryVO(userSalaryVO);
		}

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(salaryProcessVO);
		return data;
	}

	@GetMapping("/getProcess/{id}")
	@ResponseBody
	public ResultData getProcess(@PathVariable String id) {
		SalaryTemplate template = salaryTemplateRepository.findById(Integer.parseInt(id));
		SalaryProcess oldprocess = salaryProcessRepository.findByTempId(id);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(oldprocess);
		return data;
	}

	public String getSalaryValue(SalaryItem salaryItem, String json) {
		System.err.println("数据来源  ========  [" + salaryItem.getReffieldColumn() + "   值[" + json + "]");

		Map map = JSON.parseObject(json, Map.class);

		System.out.println("使用JSON类，指定解析类，解析json字符串：");
		Object obj = map.get(salaryItem.getReffieldColumn());
		String value = null;
		if (obj instanceof BigDecimal) {
			value = obj.toString();
		} else {
			value = (String) map.get(salaryItem.getReffieldColumn());
		}

		// salaryItem.setFieldValue(value);

		System.err.println("数据来源  getReffieldColumn[" + salaryItem.getReffieldColumn() + "   值===========["
				+ salaryItem.getFieldValue() + "]");

		return value;
	}

	/*
	 * hash---计算数据源，list 待统计项
	 */
	public void cal2(Map temphash, List<SalaryItem> list) {

		// list.stream()

		// Map maps2 = JSON.parseObject(user.getSalary(),Map.class);
		// 统计列
		Map srcthash = new Hashtable<>();

		// for (SalaryItem salaryItem2 : list) {
		// desthash.put(salaryItem2.getFieldCode(),
		// salaryItem2.getFieldValue());
		// }
		//
		// if (SalaryStatisticsType.totalWagePayable.getValue()
		// .equalsIgnoreCase((String)desthash.get(SalaryStatisticsType.totalWagePayable.getValue()))){
		// }
		//
		double basesalary = Double.parseDouble((String) temphash.get(ColumnType.basesalary.getValue()) == null ? "0"
				: (String) temphash.get(ColumnType.basesalary.getValue()));
		double floatsalary = Double.parseDouble((String) temphash.get(ColumnType.floatsalary.getValue()) == null ? "0"
				: (String) temphash.get(ColumnType.floatsalary.getValue()));

		// 日工资
		double daypay = (basesalary + floatsalary) / 22;

		double hourpay = daypay / 8;

		for (SalaryItem salaryItem2 : list) {

			if (salaryItem2.getFieldCode() != null) {
				if (SalaryStatisticsType.totalWagePayable.getValue().equalsIgnoreCase(salaryItem2.getFieldCode())) {

					double bonus = Double.parseDouble((String) temphash.get(ColumnType.bonus.getValue()) == null ? "0"
							: (String) temphash.get(ColumnType.bonus.getValue()));
					double allowance = Double.parseDouble((String) temphash.get(ColumnType.allowance.getValue()) == null
							? "0" : (String) temphash.get(ColumnType.allowance.getValue()));

					double overtimesalary = Double
							.parseDouble((String) srcthash.get(ColumnType.jiaban.getValue()) == null ? "0"
									: (String) temphash.get(ColumnType.jiaban.getValue()));

					// 其他加项
					double othersplus = Double
							.parseDouble((String) temphash.get(ColumnType.othersplus.getValue()) == null ? "0"
									: (String) temphash.get(ColumnType.othersplus.getValue()));
					// 累加
					double totalWagePayable = basesalary + floatsalary + overtimesalary + bonus + allowance
							+ othersplus;

					salaryItem2.setFieldValue(String.valueOf(CalculationUtil.formatDouble2(totalWagePayable)));
					srcthash.put(SalaryStatisticsType.totalWagePayable.getValue(), String.valueOf(totalWagePayable));

					// 保存统计项
					salaryItemRepository.save(salaryItem2);

					// CalculationUtil.formatDouble2(plan.getHouseFundsCompanyRatio()
					// * RAITO * insurance.getBasefee())
					// 扣减累计
				} else if (SalaryStatisticsType.totalDeduct.getValue().equalsIgnoreCase(salaryItem2.getFieldCode())) {

					double queqin = Double.parseDouble((String) temphash.get(ColumnType.queqin.getValue()) == null ? "0"
							: (String) temphash.get(ColumnType.queqin.getValue()));
					double deductattendance = CalculationUtil.formatDouble2(daypay * 0.8 * queqin);
					// 其他减项
					double othersdeduct = Double
							.parseDouble((String) temphash.get(ColumnType.othersdeduct.getValue()) == null ? "0"
									: (String) temphash.get(ColumnType.othersdeduct.getValue()));

					double total = CalculationUtil.formatDouble2(deductattendance + othersdeduct);

					srcthash.put(SalaryStatisticsType.totalDeduct.getValue(), String.valueOf(total));

					salaryItem2.setFieldValue(String.valueOf(total));
					// 保存统计项
					salaryItemRepository.save(salaryItem2);

				} else if (SalaryStatisticsType.overtimesalary.getValue()
						.equalsIgnoreCase(salaryItem2.getFieldCode())) {

					double jiaban = Double.parseDouble((String) temphash.get(ColumnType.jiaban.getValue()) == null ? "0"
							: (String) temphash.get(ColumnType.jiaban.getValue()));
					double overtimesalary = daypay * 1.5 * jiaban;
					double total = CalculationUtil.formatDouble2(overtimesalary);
					srcthash.put(ColumnType.jiaban.getValue(), String.valueOf(overtimesalary));
					salaryItem2.setFieldValue(String.valueOf(total));
					// 保存统计项
					salaryItemRepository.save(salaryItem2);

				} else if (SalaryStatisticsType.dayPay.getValue().equalsIgnoreCase(salaryItem2.getFieldCode())) {
					double total = CalculationUtil.formatDouble2(daypay);
					srcthash.put(ColumnType.jiaban.getValue(), String.valueOf(total));
					salaryItem2.setFieldValue(String.valueOf(total));
					// 保存统计项
					salaryItemRepository.save(salaryItem2);

				} else if (SalaryStatisticsType.hourPay.getValue().equalsIgnoreCase(salaryItem2.getFieldCode())) {
					double total = CalculationUtil.formatDouble2(hourpay);
					salaryItem2.setFieldValue(String.valueOf(total));
					// 保存统计项
					salaryItemRepository.save(salaryItem2);

				}

			}
		}

		// 税后
		for (SalaryItem salaryItem2 : list) {

			if (salaryItem2.getFieldCode() != null) {
				if (SalaryStatisticsType.taxPayable.getValue().equalsIgnoreCase(salaryItem2.getFieldCode())) {

					System.err.println("taxPayable=====");

					double totalDeduct = Double
							.parseDouble((String) srcthash.get(SalaryStatisticsType.totalDeduct.getValue()) == null
									? "0" : (String) srcthash.get(SalaryStatisticsType.totalDeduct.getValue()));

					double totalWagePayable = Double.parseDouble(
							(String) srcthash.get(SalaryStatisticsType.totalWagePayable.getValue()) == null ? "0"
									: (String) srcthash.get(SalaryStatisticsType.totalWagePayable.getValue()));

					double totalPerson = Double
							.parseDouble((String) temphash.get(ColumnType.totalPerson.getValue()) == null ? "0"
									: (String) temphash.get(ColumnType.totalPerson.getValue()));

					double houseFundsPersonalNum = Double
							.parseDouble((String) temphash.get(ColumnType.houseFundsPersonalNum.getValue()) == null
									? "0" : (String) temphash.get(ColumnType.houseFundsPersonalNum.getValue()));

					double total = totalWagePayable - houseFundsPersonalNum - totalPerson - totalDeduct - 5000;

					double taxPayable = CalculationUtil.formatDouble2(total);

					salaryItem2.setFieldValue(String.valueOf(taxPayable));

					srcthash.put(SalaryStatisticsType.taxPayable.getValue(), String.valueOf(taxPayable));
					// 保存统计项
					salaryItemRepository.save(salaryItem2);

				} else if (SalaryStatisticsType.totalTax.getValue().equalsIgnoreCase(salaryItem2.getFieldCode())) {

					List<SalaryPersonalTaxPlan> taxs = salaryPersonalTaxplanRepository.findAll();

					double taxPayable = Double
							.parseDouble((String) srcthash.get(SalaryStatisticsType.taxPayable.getValue()) == null ? "0"
									: (String) srcthash.get(SalaryStatisticsType.taxPayable.getValue()));

					for (SalaryPersonalTaxPlan salaryPersonalTaxPlan : taxs) {
						IntervalUtil fo = new IntervalUtil();
						// a.isInTheInterval("11", "[1,10)")
						String min = String.valueOf(salaryPersonalTaxPlan.getMinPayable());
						String max = String.valueOf(salaryPersonalTaxPlan.getMaxPayable());
						
						System.err.println("[" + min + "," + max + ")=========="
								+ fo.isInTheInterval(String.valueOf(taxPayable), "[" + min + "," + max + ")"));

						// 如果是这个区间，
						if (fo.isInTheInterval(String.valueOf(taxPayable), "[" + min + "," + max + ")")) {
							// 应税工资 *税率
							double total = taxPayable * salaryPersonalTaxPlan.getTaxRatio()*0.01;
							double totalTax = CalculationUtil.formatDouble2(total);
							
							System.err.println("totalTax=========="
									+ totalTax);
							salaryItem2.setFieldValue(String.valueOf(totalTax));
							srcthash.put(SalaryStatisticsType.totalTax.getValue(), totalTax);
							// 保存统计项
							salaryItemRepository.save(salaryItem2);
						}
					}

					// 应税工资 *税率
					// double total = taxPayable*0.1;
					//
					// double totalTax = CalculationUtil.formatDouble2(total);
					// salaryItem2.setFieldValue(String.valueOf(totalTax));
					// srcthash.put(SalaryStatisticsType.totalTax.getValue(),
					// totalTax);
					// // 保存统计项
					// salaryItemRepository.save(salaryItem2);

				}
			}
		}

	}

	public String caltSalaryValue(SalaryItem salaryItem, String json) {
		System.err.println("数据来源       类型=" + salaryItem.getRulesType() + "  自定义计算公式=[" + salaryItem.getRules() + "]");
		// 自定义公式
		// 原公式 a=(b-c)/(d-c)
		// &a&=&(&b&-&c&)&/&(&d&-&c&)&
		String val = salaryItem.getRules();
		// 分解计算公式 ，得到变量名称
		String[] aa1 = val.split("&");
		// 去掉转义符号
		String newstr = val.replace("&", "");
		// 构造计算器
		ExpressRunner runner = new ExpressRunner(false, false);
		DefaultContext<String, Object> context = new DefaultContext<String, Object>();

		// 拼接公式
		String eq = "a=" + newstr;

		// 循环动态赋值
		// for (String string : aa1) {
		// //
		// System.out.println(string+"===="+CalculationUtil.isNumericZidai(string));
		//
		// // 检查是否是字符串，此处能不用id，需要用name，id无法注入规则引擎
		// if (string != null && string.length() > 0 &&
		// CalculationUtil.check(string)) {
		// // 获取基本薪资数据
		// // UserField userfield =
		// // userFieldRepository.findByNameUserId(string,
		// // String.valueOf(user.getId()));
		//
		// // 过滤条件 人员，进程，列名
		//// SalaryItem item = salaryItemRepository.findByColAndPId(string,
		//// String.valueOf(user.getId()), String.valueOf(process.getId()));
		//
		// System.err.println("item=====" + item);
		//
		// if (item != null) {
		// System.err.println("注入字段变量=" + string + " 注入值=[" +
		// item.getFieldValue() + "]");
		//
		// context.put(string, new BigDecimal(item.getFieldValue()));
		// // context.put("c", new
		// // BigDecimal("0.15384615384615385"));
		// // context.put("d", new
		// // BigDecimal("1"));
		// }
		// }
		// }
		// Object r = null;
		// try {
		// System.err.println(" 转义前=【" + salaryitem.getRules() + "】 计算公式=" +
		// eq);
		// r = runner.execute(eq, context, null, false, false);
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// // 设置计算结果
		// System.err.println("设置计算结果 =" + r.toString());
		// snapshot.setFieldValue(r.toString());
		// //
		//
		// // 此处为导入

		return null;
	}

	@ResponseBody
	@RequestMapping("/updateSalaryItem")
	public ResultData updateSalaryItem(@RequestBody SalaryItem message) {
		// addOrUpdate(message);
		// 查找表单所有字段
		/// List<WfwFormField> wfwFormFields =
		// wfwFormFieldRepository.findByflowId(id);
		salaryItemRepository.save(message);
		// 绑定规则字段到活动上
		// UPDATE t_wfw_form_field SET activity_id =?,rules =? WHERE id =?
		System.err.println(message.getId() + "  field " + message.getId() + "  rules " + message.getRules());
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(message);
		return data;

	}

	// @RequestMapping("/listusersalary")
	@GetMapping("/delProcess/{id}")
	@ResponseBody
	public ResultData delProcess(@PathVariable String id) throws Exception {

		salaryProcessRepository.delete(salaryProcessRepository.findById(Integer.parseInt(id)));
		
		
		
		salaryItemRepository.delAccount(Integer.parseInt(id));

		// salaryProcessRepository.
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(id);
		return data;

	}
}