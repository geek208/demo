package com.hadron.wfw.api;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hadron.wfw.GetPayNo;
import com.hadron.wfw.HttpAPIService;
import com.hadron.wfw.KeycloakUtils;
import com.hadron.wfw.PayStatus;
import com.hadron.wfw.RedisService;
import com.hadron.wfw.ResultData;
import com.hadron.wfw.cache.UserCache;
import com.hadron.wfw.model.EasyPay;
import com.hadron.wfw.model.ScoreCard;
import com.hadron.wfw.model.UserVO;
import com.hadron.wfw.producer.Producer;
import com.hadron.wfw.service.EasyPayRepository;
import com.hadron.wfw.service.EasyPayService;
import com.hadron.wfw.service.ScoreCardRepository;

import io.swagger.annotations.ApiOperation;

/**
 * User controller
 * <p/>
 * Created in 2018.11.16
 * <p/>
 *
 */
@Controller
@RequestMapping("/pay")
public class PayController {

    /**
     * The User service.
     */
    @Autowired
    EasyPayService payService;
    
    @Autowired
	UserCache userCache;
    
    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    private EasyPayRepository payRepository;
    
    @Autowired
    private ScoreCardRepository scoreCardRepository;
    
    
//    @Autowired
//    private ScoreCardRepository scoreCardRepository;
    
    @Autowired
    private HttpAPIService httpAPIService;
    @Autowired
    private Producer producer;
//    @Autowired
//    private KafkaTemplate kafkaTemplate;
	@Autowired
	private RedisService redisService;
	@Autowired
	private Environment env;
	
    private static Gson gson = new GsonBuilder().create();
    /**
     * Add string.
     *
     * @param user the user
     * @return the string
     */
    @RequestMapping("/createOrder")
    @ResponseBody
    //@RequestBody http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1
    public ResultData createOrder(@RequestBody EasyPay pay) {
    	
    	
        pay.setOrderSn(GetPayNo.getOrderNo());
        pay.setCreateDate(new Date());
        pay.setUpdateDate(new Date());
        
    	//payService.save(pay);
    	
    	
//        Message message = new Message();
//        message.setId("KFK_STOCK"+System.currentTimeMillis());
//        message.setMsg(order.getName());
//        message.setSendTime(new Date());
    	
    	//curl -X POST "http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1" -H  "accept: */*"
    	//http://192.168.1.131:8080/createOrder?payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1
    	//http://192.168.1.131:8080/createOrder?
    	//payId=1&param=1&type=1&price=0.01&notifyUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&returnUrl=http%3A%2F%2F192.168.1.123%3A9091%2FsuccessNotice&sign=1&isHtml=1

    	String result = null;
    	
    	//UrlEncode util =new UrlEncode();
    	
		try {
			result = httpAPIService.doGet(env.getProperty("spring.bizmate.pay")
					+ "/createOrder?payId="+pay.getOrderSn()+"&param="+pay.getParam()
					+ "&type="+pay.getType()+"&price="+pay.getAmount()+""
					+ "&notifyUrl="+env.getProperty("spring.bizmate.noticeUrl")
					+ "&returnUrl="+env.getProperty("spring.bizmate.returnUrl")
					+ "&sign=1"
					+ "&isHtml=1");
			
			
			System.err.println("req="+env.getProperty("spring.bizmate.pay")
			+ "/createOrder?payId="+pay.getOrderSn()+"&param="+pay.getParam()
			+ "&type="+pay.getType()+"&price="+pay.getAmount()+""
			+ "&notifyUrl="+env.getProperty("spring.bizmate.noticeUrl")
			+ "&returnUrl="+env.getProperty("spring.bizmate.returnUrl")
			+ "&sign=1"
			+ "&isHtml=1");
			System.err.println("result="+result);
			
			pay.setUrl(result);
			pay.setReturnUrl(env.getProperty("spring.bizmate.returnUrl"));
			payService.save(pay);
			
//			ResultData data = gson.fromJson(result, ResultData.class);
//			    //如果回调成功
//			      String sucesss  = (String) data.getData();
//			      if(PayStatus.PAYSUCESS.getValue().equalsIgnoreCase(sucesss)){
//			    	  System.err.println("支付回调结果"+sucesss);
//			    	  //payRepository.updatePayStatus(orderSn, status);
//			    }else{
//			    	new Exception("更新支付异常");
//			    }
		} catch (Exception e) {
			e.printStackTrace();
		}
			

        ResultData data =new ResultData();
        data.setCode(200);
        data.setSuccess(true);
        data.setMessage("成功");
        data.setData(pay);
        return data;
    }
    
    
    
    /**
     * Add string.
     *
     * @param user the user
     * @return the string
     * 
     * http://192.168.1.131:8082/pay/notice?orderSn=P2022102217402580187&status=1
     */
    @RequestMapping("/getPayStatus")
    @ResponseBody
    public String  getPayStatus(String payId,String state) {
    	
    	
           System.err.println("notice  {payid=========== "+ payId+"state====== "+state+"}");
    	
//        Message message = new Message();
//        message.setId("KFK_STOCK"+System.currentTimeMillis());
//        
//        message.setMsg(order.getName());
//        message.setSendTime(new Date());
        try {
    		//producer.send();
    		  /// redisService.set(message.getId(), message.getMsg());
    	 	   //System.err.print("send kfk express="+gson.toJson(message));
    	       // kafkaTemplate.send("mall", gson.toJson(message));
        	    //通知支付成功
    	        //kafkaTemplate.send("order", gson.toJson(message));
		    	//更新支付状态
        	   //payRepository.getOne(arg0)
//			    String result;
//				try {
//					result = httpAPIService.doGet(env.getProperty("spring.bizmate.order")+"/callbackPay?orderSn="+orderSn+"&payStatus=1");
//					ResultData data = gson.fromJson(result, ResultData.class);
//					    //如果回调成功
//					      String sucesss  = (String) data.getData();
//					      if(PayStatus.PAYSUCESS.getValue().equalsIgnoreCase(sucesss)){
//					    	  System.err.println("支付回调结果"+sucesss);
//					    	  //payRepository.updatePayStatus(orderSn, status);
//					    }else{
//					    	new Exception("更新支付异常");
//					    }
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
        	    
    	        //通知待发货
    	       // kafkaTemplate.send("express", gson.toJson(message));
			    //httpAPIService.doGet(env.getProperty("spring.bizmate.express")+"/addExpress?id="+order.getId()+"&name="+order.getName() +"&money=2&fee=2");
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

//        ResultData data =new ResultData();
//        data.setCode(200);
//        data.setSuccess(true);
//        data.setMessage("成功");
//        data.setData(payId);
        return "success";
    }
    
    
    /**
     * Add string.
     *
     * @param user the user
     * @return the string
     * 
     * http://192.168.1.131:8082/pay/notice?orderSn=P2022102217402580187&status=1
     */
    @RequestMapping("/notice")
    @ResponseBody
    public ResultData  notice(String payId,String state,String price ,String reallyPrice) {
    	
    	
           System.err.println("notice  {payid=========== "+ payId+"state====== "+state+"}");
    	
//        Message message = new Message();
//        message.setId("KFK_STOCK"+System.currentTimeMillis());
//        
//        message.setMsg(order.getName());
//        message.setSendTime(new Date());
        try {
    		//producer.send();
    		  /// redisService.set(message.getId(), message.getMsg());
    	 	   //System.err.print("send kfk express="+gson.toJson(message));
    	       // kafkaTemplate.send("mall", gson.toJson(message));
        	    //通知支付成功
    	        //kafkaTemplate.send("order", gson.toJson(message));
		    	//更新支付状态
        	
		    	payService.updatePayStatus(payId, state);
		    	
		    	//payRepository.f
		    	
		       String username = KeycloakUtils.getKeycloakSecurityContext(request);
		  		
		  		UserVO cache = userCache.getCache(username);
		    	ScoreCard  sc =new ScoreCard();
    			sc.setUserId(String.valueOf(cache.getId()));
    			
    			//积分等于充值价格 乘 10
    	        sc.setAmount(Double.valueOf(reallyPrice)*10);
		    	        
		    	scoreCardRepository.save(sc);
//			    String result;
//				try {
//					result = httpAPIService.doGet(env.getProperty("spring.bizmate.order")+"/callbackPay?orderSn="+orderSn+"&payStatus=1");
//					ResultData data = gson.fromJson(result, ResultData.class);
//					    //如果回调成功
//					      String sucesss  = (String) data.getData();
//					      if(PayStatus.PAYSUCESS.getValue().equalsIgnoreCase(sucesss)){
//					    	  System.err.println("支付回调结果"+sucesss);
//					    	  //payRepository.updatePayStatus(orderSn, status);
//					    }else{
//					    	new Exception("更新支付异常");
//					    }
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
        	    
    	        //通知待发货
    	       // kafkaTemplate.send("express", gson.toJson(message));
			    //httpAPIService.doGet(env.getProperty("spring.bizmate.express")+"/addExpress?id="+order.getId()+"&name="+order.getName() +"&money=2&fee=2");
		} catch (Exception e) {
			e.printStackTrace();
		}

        ResultData data =new ResultData();
        data.setCode(200);
        data.setSuccess(true);
        data.setMessage("成功");
        data.setData(payId);
        return data;
    }
    
    
    /**
     * Add string.
     *
     * @param user the user
     * @return the string
     */
    @RequestMapping("/pay")
    @ResponseBody
    public ResultData pay(String orderSn,String status) {
    	
    	

    	
//        Message message = new Message();
//        message.setId("KFK_STOCK"+System.currentTimeMillis());
//        
//        message.setMsg(order.getName());
//        message.setSendTime(new Date());
        try {
    		//producer.send();
    		  /// redisService.set(message.getId(), message.getMsg());
    	 	   //System.err.print("send kfk express="+gson.toJson(message));
    	       // kafkaTemplate.send("mall", gson.toJson(message));
        	    //通知支付成功
    	        //kafkaTemplate.send("order", gson.toJson(message));
		    	//更新支付状态
		    	payService.updatePayStatus(orderSn, status);
//			    String result;
//				try {
//					result = httpAPIService.doGet(env.getProperty("spring.bizmate.order")+"/callbackPay?orderSn="+orderSn+"&payStatus=1");
//					ResultData data = gson.fromJson(result, ResultData.class);
//					    //如果回调成功
//					      String sucesss  = (String) data.getData();
//					      if(PayStatus.PAYSUCESS.getValue().equalsIgnoreCase(sucesss)){
//					    	  System.err.println("支付回调结果"+sucesss);
//					    	  //payRepository.updatePayStatus(orderSn, status);
//					    }else{
//					    	new Exception("更新支付异常");
//					    }
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
        	    
    	        //通知待发货
    	       // kafkaTemplate.send("express", gson.toJson(message));
			    //httpAPIService.doGet(env.getProperty("spring.bizmate.express")+"/addExpress?id="+order.getId()+"&name="+order.getName() +"&money=2&fee=2");
		} catch (Exception e) {
			e.printStackTrace();
		}

        ResultData data =new ResultData();
        data.setCode(200);
        data.setSuccess(true);
        data.setMessage("成功");
        data.setData(orderSn);
        return data;
    }

	/**
	 * Add string.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/getEasyPayList")
	@ApiOperation("支付单列表")
	@ResponseBody
	// @CrossOrigin(originPatterns = "*", methods = {RequestMethod.GET,
	// RequestMethod.POST})
	public ResultData getEasyPayList() throws Exception {

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");

		// ArrayList list = new ArrayList();
		// list.add(mallService.findOrderById(id));

		data.setData(payRepository.findAll());
		
		return data;
	}
}