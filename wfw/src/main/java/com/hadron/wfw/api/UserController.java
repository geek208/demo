package com.hadron.wfw.api;



import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hadron.app.PlatformApp;
import com.hadron.app.PlatformAppRepository;
import com.hadron.wfw.JsonUtil;
import com.hadron.wfw.KeycloakUtils;
import com.hadron.wfw.Result;
import com.hadron.wfw.ResultData;
import com.hadron.wfw.Utils;
import com.hadron.wfw.cache.UserCache;
import com.hadron.wfw.model.SysUser;
import com.hadron.wfw.model.UserField;
import com.hadron.wfw.model.UserVO;
import com.hadron.wfw.model.WfwActivity;
import com.hadron.wfw.model.WfwActivityRules;
import com.hadron.wfw.model.WfwFlow;
import com.hadron.wfw.model.WfwRole;
import com.hadron.wfw.model.WfwUser;
import com.hadron.wfw.service.UserFieldRepository;
import com.hadron.wfw.service.UserRepository;
import com.hadron.wfw.service.UserService;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

//import org.keycloak.KeycloakPrincipal;
//import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.security.Principal;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 9db06bcff9248837f86d1a6bcf41c9e7
 * @author xuyuchao
 *
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PlatformAppRepository platformAppRepository;
    
    @Autowired
   	private UserFieldRepository userFieldRepository;
    
    @Autowired
    private HttpServletRequest request;
    
//    @Autowired
//    KeycloakTokenFilter keycloakTokenFilter;

    @Autowired
    UserService userService;

    @Autowired
    UserCache userCache;

    public static final String COOKIE_NAME = "Admin-Token";
    
    @PostMapping("/login")
    public Result<UserVO> login(@RequestBody UserVO user, HttpServletRequest request, HttpServletResponse response) {
    	
    	log.debug("pre md5Hex{} after {}"+user.getPassword(),DigestUtils.md5Hex(StringUtils.trim(user.getPassword())));
        user.setPassword(DigestUtils.md5Hex(StringUtils.trim(user.getPassword())));
        
        SysUser loginUser = userService.login(user);
        UserVO userVO = loginUser.toVO(UserVO.class);
        String cKey = Utils.getCharAndNum(10);
        Cookie cookie = new Cookie(COOKIE_NAME, cKey);
        // cookie.setDomain(domain);
        //12个小时
        cookie.setMaxAge(60 * 60 * 12);
        cookie.setPath("/");
        // xss
     //   cookie.setHttpOnly(true);
      //  response.addHeader("Access-Control-Allow-Credentials", "true");
        response.addCookie(cookie);
        System.out.println(response.toString());
        
        userCache.setCache(cKey, userVO);
    	
//        ResultData data =new ResultData();
//        data.setCode(20000);
//        data.setSuccess(true);
//        data.setMessage("成功");
//        
//        data.setData("admin-token");
//        return data;

        return Result.buildSuccess(userVO, null);
    }


    @GetMapping("/logout")
    public Result logout(@CookieValue(value = COOKIE_NAME, defaultValue = "") String auth, HttpServletResponse response) {
        if (auth != null) {
            Cookie cookie = new Cookie(COOKIE_NAME, "");
            cookie.setMaxAge(0);
            cookie.setPath("/");
            response.addCookie(cookie);
        }
        userCache.rmCache(auth);
        return Result.doSuccess();
    }
    
    /**
	 * 获取表单
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/userList")
	@ResponseBody
	public ResultData getUserList(HttpServletRequest request) {

		List<SysUser> users = userRepository.findAll();

		// WfwFormV formV =new WfwFormV();
		// formV.setFormfield(formField);
		 //String token = authorization.replace("Bearer ", "");
	        
	  // IDToken id = SecurityUtils.getIDToken(request);
	        
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(users);
		return data;
	}
	
    /**
	 * 获取表单
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@RequestMapping("/appList")
	@ResponseBody
	public ResultData getAppList() {

		List<PlatformApp> users = platformAppRepository.findAll();

		// WfwFormV formV =new WfwFormV();
		// formV.setFormfield(formField);
		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(users);
		return data;
	}
	
	 /**
     * Add string.
     *
     * @param user the user
     * @return the string
     * @throws Exception 
     */
    @RequestMapping("/createApp")
	@ResponseBody
    public ResultData createApp(PlatformApp role) throws Exception {
    	platformAppRepository.save(role);
       // producer.send();
        
   	    // 声明httpPost请求
        //HttpPost httpPost = new HttpPost("http://www.baidu.com");
		//httpClient.execute(httpPost);
		//httpAPIService.doGet("http://www.baidu.com");
		//httpAPIService.doGet("http://10.0.0.79:30093/apm/add?id="+user.getId()+"&userName="+user.getUserName()+"&password=1&age=1");
		//httpAPIService.doGet("http://10.0.0.79:30095/mall/addOrder?id="+user.getId()+"&name="+user.getUserName()+"&money=2&fee=2");
        //return gson.toJson("200");
    	ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(role);
		return data;
     
    }
    
    /**
     * cache
     *
     * @param auth
     * @return
     */
    // @PreAuth("vip")
    @GetMapping("/info")
    public Result getUserInfo(@CookieValue(value = COOKIE_NAME, defaultValue = "") String auth) {
        if (StringUtils.isBlank(auth)) return Result.builder().code(403).message("无权限").build();
       
        UserVO cache = userCache.getCache(auth);
        if (cache == null) return Result.builder().code(403).message("认证已经过期，请重新登录").build();
        Map<String, Object> userMap = Maps.newHashMap();
        userMap.put("roles", Lists.newArrayList(cache.getRole()));
        userMap.put("name", cache.getEmail());
        //userMap.put("avatar", "https://apic.douyucdn.cn/upload/avanew/face/201705/15/17/109dae304969a8dc9dfd318c34cb71e9_middle.jpg");
        return Result.builder().code(Result.CODE_SUCCESS).obj(userMap).build();
    }
    
    
    /**
     * cache
     *
     * @param auth
     * @return
     */
    // @PreAuth("vip")
    @GetMapping("/info2")
    @ResponseBody
    public Result getUserInfo2(@CookieValue(value = COOKIE_NAME, defaultValue = "") String auth,HttpServletRequest request) {
        
    	Map<String, Object> userMap = Maps.newHashMap();
    	System.err.println(COOKIE_NAME+"   = "+auth);
    	//KeycloakSecurityContext sc=  null; //this.getKeycloakSecurityContext();
    	//if(sc !=null){
    	  String  username =  	KeycloakUtils.getKeycloakSecurityContext(request);
    	  
    	  UserVO cache = userCache.getCache(username);
//    	  if(cache ==null){
//    		  UserVO vo = new UserVO();
//    		  vo.setUsername(username);
//    		  SysUser loginUser = userRepository.findUserByName(username);
//    		  UserVO userVO = loginUser.toVO(UserVO.class);
//    		  userCache.setCache(username, userVO);
//    		  
//    		  cache = userCache.getCache(username);
//    	  }
    	  
    	  if (cache == null) return Result.builder().code(403).message("认证已经过期，请重新登录").build();
          userMap.put("roles", Lists.newArrayList(cache.getRole()));
          userMap.put("name", cache.getUserName());
    	//}
    	
        if (StringUtils.isBlank(auth)) return Result.builder().code(403).message("无权限").build();
        //userMap.put("avatar", "https://apic.douyucdn.cn/upload/avanew/face/201705/15/17/109dae304969a8dc9dfd318c34cb71e9_middle.jpg");
        return Result.builder().code(Result.CODE_SUCCESS).obj(userMap).build();
    }
    
    
    /**
     * 用户登录成功之后，获取用户信息
     * 1.获取用户id
     * 2.根据用户id查询用户
     * 3.构建返回值对象
     * 4.响应
     *
     * @param request
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/oa")
    public Result profile(HttpServletRequest request) throws  Exception {

        /**
         * 从请求头信息中获取token数据
         *   1.获取请求头信息：名称=Authorization(前后端约定)
         *   2.替换Bearer+空格
         *   3.解析token
         *   4.获取clamis
         */
//    	
 //   	JwtUtils jwtUtils =new JwtUtils();
//    	
//        //1.获取请求头信息：名称=Authorization(前后端约定)
        //String authorization = request.getHeader("X-Token");
        String authorization = request.getHeader("Authorization");
        
        System.err.println("authorization="+authorization);
        if (StringUtils.isEmpty(authorization)) {
//            throw new PendingException(ResCode.UNAUTHENTICATED);
            //系统未捕捉到请求头信息
            throw new Exception(" not Authorization");
        }
        
        //2.替换Bearer+空格
        String token = authorization.replace("Bearer ", "");
        
        //IDToken id = SecurityUtils.getIDToken(request);
        
        
//        System.err.println("IDToken="+JsonUtil.getJsonString(id));
      


        //3.解析token
    //    Claims claims = jwtUtils.parseJwt(token);
        //4.获取clamis
       // String userId = claims.getId();
        
        //String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXUyJ9.eyJpc3MiOiJhdXRoMCJ9.AbIJTDMFc7yUa5MhvcP03nJPyCPzZtQcGEp-zWfOkEE";
//		    RSAPublicKey publicKey = null;   //Get the key instance
//		    RSAPrivateKey privateKey = null;  //Get the key instance
//		    try {
//		        Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
//		        JWTVerifier verifier = JWT.require(algorithm)
//		            .withIssuer("auth0")
//		            .build(); //Reusable verifier instance
//		        DecodedJWT jwt = verifier.verify(token);
        
//		    } catch (JWTVerificationException exception){
//		        //Invalid signature/claims
//		    }
//
//        //  String userId = "U01";
//        UserVO cache = userCache.getCache(authorization);
////
////        /**此处只是为了获取token中的用户数据，所有只简单返回用户对象，
////         * 工作则按实际要求多表查询需要数据（根据用户ID查询权限）
////         */
////
////        //return new Result(ResultCode.SUCCESS, user);
//       // if (cache == null) return Result.builder().code(403).message("认证已经过期，请重新登录").build();
//        Map<String, Object> userMap = Maps.newHashMap();
//        userMap.put("roles", "admin");
//        userMap.put("name", "13800138004");
        
        //userService
    	
    	return Result.builder().code(Result.CODE_SUCCESS).obj("hello OA").build();
    }
    
    
    
    /**
     * 用户登录成功之后，获取用户信息
     * 1.获取用户id
     * 2.根据用户id查询用户
     * 3.构建返回值对象
     * 4.响应
     *
     * @param request
     * @return
     * @throws Exception
     */
    @ResponseBody
    @GetMapping(value = "/mall")
    public Result profile3(HttpServletRequest request) throws  Exception {

        /**
         * 从请求头信息中获取token数据
         *   1.获取请求头信息：名称=Authorization(前后端约定)
         *   2.替换Bearer+空格
         *   3.解析token
         *   4.获取clamis
         */
//    	
    	//JwtUtils jwtUtils =new JwtUtils();
//    	
//        //1.获取请求头信息：名称=Authorization(前后端约定)
        //String authorization = request.getHeader("X-Token");
        String authorization = request.getHeader("Authorization");
        System.err.println("authorization="+authorization);
    
        //2.替换Bearer+空格
        String token = authorization.replace("Bearer ", "");
        
//        IDToken id = SecurityUtils.getIDToken(request);
//        System.err.println("IDToken="+JsonUtil.getJsonString(id));

        //3.解析token
       //Claims claims = jwtUtils.parseJwt(token);
//        //4.获取clamis
//        String userId = claims.getId();
//
//        //  String userId = "U01";
//        UserVO cache = userCache.getCache(authorization);
////
////        /**此处只是为了获取token中的用户数据，所有只简单返回用户对象，
////         * 工作则按实际要求多表查询需要数据（根据用户ID查询权限）
////         */
////
////        //return new Result(ResultCode.SUCCESS, user);
//       // if (cache == null) return Result.builder().code(403).message("认证已经过期，请重新登录").build();
//        Map<String, Object> userMap = Maps.newHashMap();
//        userMap.put("roles", "admin");
//        userMap.put("name", "13800138004");
        
        //userService
    	
    	return Result.builder().code(Result.CODE_SUCCESS).obj("hello mall").build();
    }
    
    
//    @RequestMapping(value = "/products",  method = RequestMethod.GET)
//	public Result handleCustomersRequest(Principal principal) {
//    	
//    	 if (principal instanceof KeycloakPrincipal) {
//    	        AccessToken accessToken = ((KeycloakPrincipal) principal).getKeycloakSecurityContext().getToken();
//    	        String preferredUsername = accessToken.getPreferredUsername();
//    	        AccessToken.Access realmAccess = accessToken.getRealmAccess();
//    	        Set<String> roles = realmAccess.getRoles();
//    	        log.info("当前登录用户：{}, 角色：{}", preferredUsername, roles);
//    	    }
//
//    	
//    	return Result.builder().code(Result.CODE_SUCCESS).obj("product").build();
//	}

    
    
    /**
     * 用户登录成功之后，获取用户信息
     * 1.获取用户id
     * 2.根据用户id查询用户
     * 3.构建返回值对象
     * 4.响应
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/userinfo", method = RequestMethod.GET)
    public Result profile2(HttpServletRequest request) throws  Exception {

        /**
         * 从请求头信息中获取token数据
         *   1.获取请求头信息：名称=Authorization(前后端约定)
         *   2.替换Bearer+空格
         *   3.解析token
         *   4.获取clamis
         */
    	
    	//JwtUtils jwtUtils =new JwtUtils();
    	
        //1.获取请求头信息：名称=Authorization(前后端约定)
        String authorization = request.getHeader("Authorization");
        if (StringUtils.isEmpty(authorization)) {
//            throw new PendingException(ResCode.UNAUTHENTICATED);
            //系统未捕捉到请求头信息
            throw new Exception(" not Authorization");
        }
        //2.替换Bearer+空格
        String token = authorization.replace("Bearer ", "");

        //3.解析token
     //   Claims claims = jwtUtils.parseJwt(token);
        //4.获取clamis
        //String userId = claims.getId();

        //  String userId = "U01";
        UserVO cache = userCache.getCache(token);

        /**此处只是为了获取token中的用户数据，所有只简单返回用户对象，
         * 工作则按实际要求多表查询需要数据（根据用户ID查询权限）
         */

        //return new Result(ResultCode.SUCCESS, user);
        if (cache == null) return Result.builder().code(403).message("认证已经过期，请重新登录").build();
        Map<String, Object> userMap = Maps.newHashMap();
        userMap.put("roles", Lists.newArrayList(cache.getRole()));
        userMap.put("name", cache.getEmail());
        
        //userService
    	return Result.builder().code(Result.CODE_SUCCESS).obj(userMap).build();
    }
    

    
    
    /**
     * 用户登录
     * 1.通过service根据mobile查询用户
     * 2.比较password
     * 3.生成jwt信息
     *
     * @param loginMap
     * @return
     * @requestBody把请求数据封装(前端以json方式传)
     */
    @RequestMapping(value = "/login2", method = RequestMethod.POST)
    public Result login2(@RequestBody UserVO user, HttpServletRequest request, HttpServletResponse response) {
    	
//    	JwtUtils jwtUtils =new JwtUtils();
    	
    	log.debug("pre md5Hex{} after {}"+user.getPassword(),DigestUtils.md5Hex(StringUtils.trim(user.getPassword())));
        user.setPassword(DigestUtils.md5Hex(StringUtils.trim(user.getPassword())));
        
        SysUser loginUser = userService.login(user);
        
//        String mobile = loginMap.get("mobile");
//        String password = loginMap.get("password");
        
       // User user = userService.selectByMobile(mobile);
        //登录失败
//        if (user == null || !user.getPassword().equals(password)) {
//            //既可以使用抛异常，也可使用直接返回错误码(推荐)
//        	return Result.builder().code(501).obj("失败").build();
//        } else {
//            //其他数据以map集合存放在token中
//            Map<String, Object> dataMap = new HashMap<>();
////            dataMap.put("companyId", user.getCompanyId());
////            dataMap.put("companyName", user.getCompanyName());
//            //生成token并存入数据返回
//            String token = jwtUtils.createJwt(String.valueOf(loginUser.getId()), loginUser.getUserName(), dataMap);
//            return Result.builder().code(Result.CODE_SUCCESS).obj(token).build();
//            //return new Result(Result.SUCCESS(), token);
//        }
//    	
        
        //其他数据以map集合存放在token中
        Map<String, Object> dataMap = new HashMap<>();
//        dataMap.put("companyId", user.getCompanyId());
//        dataMap.put("companyName", user.getCompanyName());
        //生成token并存入数据返回
     //   String token = jwtUtils.createJwt(String.valueOf(loginUser.getId()), loginUser.getUserName(), dataMap);
        
        UserVO userVO = loginUser.toVO(UserVO.class);
        
//        userVO.setToken(token);
//        
//        userCache.setCache(token, userVO);
        
        return Result.builder().code(Result.CODE_SUCCESS).obj(userVO).build();
    	
    }
    
    
    
	@GetMapping("/getUserFieldList/{id}")
	@ResponseBody
	public ResultData getUserFieldList(@CookieValue(value = COOKIE_NAME, defaultValue = "") String auth, @PathVariable String id ) {
		
//		  if (StringUtils.isBlank(auth)) return Result.builder().code(403).message("无权限").build();
	    ///UserVO cache = userCache.getCache(auth);
	    List<UserField> users2 = userFieldRepository.findByUserId(id);
	    
	    SysUser sysUser =  userRepository.findById(Integer.parseInt(id));
	    List<UserField> users  = (List<UserField>) userFieldRepository.findByOrgId(sysUser.getOrgId());
	    
	    //如果本人已经有记录
		// 每个用户快照一份
		ArrayList<UserField> newuserField = new ArrayList();
		
		if(users2 != null&& users2.size() >0){
			
			for (UserField userField : users2) {
				userField.setUpdateDate(new Date());
				
			}
			ResultData data = new ResultData();
			data.setCode(200);
			data.setSuccess(true);
			data.setMessage("成功");
			data.setData(users2);
			return data;
		}
//	    if(users2 != null && users2.size()>0){
//	    	users = users2;
//		  
//		    }else{
//		    	 // if( cache !=null){
//					
//						for (UserField userField : users) {
//							UserField  u= new UserField();
//							u.setCnName(userField.getCnName());
//							u.setUserId(id);
//							u.setFieldType(userField.getFieldType());
//							u.setName(userField.getName());
//							u.setOrgId(userField.getOrgId());
//							u.setParentId(userField.getParentId());
//							u.setGroupId(userField.getGroupId());
//							u.setCreateDate(new Date());
//							userFieldRepository.save(u);
//							newuserField.add(u);
//						}
//		  //  }
//	    }
	    
		for (UserField userField : users) {
			UserField  u= new UserField();
			u.setCnName(userField.getCnName());
			u.setUserId(id);
			u.setFieldType(userField.getFieldType());
			u.setName(userField.getName());
			u.setOrgId(userField.getOrgId());
			u.setParentId(String.valueOf(userField.getId()));
			u.setGroupId(userField.getGroupId());
			u.setCreateDate(new Date());
			u.setUpdateDate(new Date());
			userFieldRepository.save(u);
			newuserField.add(u);
		}
//		WfwFormV formV = new WfwFormV();
//		formV.setFormfield(formField);

		ResultData data = new ResultData();
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(newuserField);
		return data;
	}
    
    /**
     * Add string.
     *
     * @param user the user
     * @return the string
     * @throws Exception 
     */
    @RequestMapping("/createUser")
	@ResponseBody
    public ResultData createUser(SysUser user) throws Exception {
    	
    	
    	log.debug("pre md5Hex{} after {}"+user.getPassword(),DigestUtils.md5Hex(StringUtils.trim(user.getPassword())));
    	
    	//user.setPassword(DigestUtils.md5Hex(DigestUtils.md5Hex(StringUtils.trim(user.getPassword()))));
    	userRepository.save(user);
    	
       // producer.send();
        
   	    // 声明httpPost请求
        //HttpPost httpPost = new HttpPost("http://www.baidu.com");
		//httpClient.execute(httpPost);
		//httpAPIService.doGet("http://www.baidu.com");
		//httpAPIService.doGet("http://10.0.0.79:30093/apm/add?id="+user.getId()+"&userName="+user.getUserName()+"&password=1&age=1");
		//httpAPIService.doGet("http://10.0.0.79:30095/mall/addOrder?id="+user.getId()+"&name="+user.getUserName()+"&money=2&fee=2");
        //return gson.toJson("200");
    	
    	
    	ResultData data = new ResultData();
    	
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(user);
		return data;
     
    }
    
    
	@ResponseBody
	@RequestMapping("/updateUser")
	public ResultData updateUser(@RequestBody SysUser message) {
		// addOrUpdate(message);
		// 查找表单所有字段
		/// List<WfwFormField> wfwFormFields =
		// wfwFormFieldRepository.findByflowId(id);
		
		message.setJson(JSON.toJSONString(message));
		//salaryInsuranceRepository.save(insurance);

		userRepository.save(message);

		// 绑定规则字段到活动上
		// UPDATE t_wfw_form_field SET activity_id =?,rules =? WHERE id =?

		System.err.println(message.getId() + "  field " + message.getUserName()+ "  rules \n " + message.getIdCardNum()+message.getJson());

//		WfwActivityRules rules = new WfwActivityRules();
//		rules.setActivityId(String.valueOf(message.getId()));
//		rules.setFieldId(message.getFieldId());
//		rules.setFlowId(message.getFlowId());
//		rules.setUserId(message.getUserId());
//		rules.setName(message.getName());
//		rules.setRules(message.getRules());
//
//		rules.setCreateDate(new Date());
		//wfwFormFieldRepository.save(rules);

		//
		// wfwFormFieldRepository.updateFieldActivity(message.getFieldId(),
		// String.valueOf(message.getId()),message.getRules());

		// bindUser(String.valueOf(message.getId()), message.getUserId(),
		// message.getUserType());
		ResultData data = new ResultData();
		
	
		
		data.setCode(200);
		data.setSuccess(true);
		data.setMessage("成功");
		data.setData(message);
		return data;

	}
	
    
    private void configCommonAttributes(Model model) {
       // model.addAttribute("identity", new Identity(getKeycloakSecurityContext()));
    }

//    private KeycloakSecurityContext getKeycloakSecurityContext() {
//    	KeycloakSecurityContext sc = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
//    	System.err.println("sc="+sc);
//        return sc;
//    }
}
