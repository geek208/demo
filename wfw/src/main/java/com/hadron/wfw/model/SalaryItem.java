package com.hadron.wfw.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;





/**
 * 
 * 
 * 
 * @author xuychao xuychao@163.com
 *
 */

@Entity
@Table(name = "t_salary_item")
@Data
@AllArgsConstructor
@NoArgsConstructor 
public class SalaryItem extends BaseEntity implements Serializable {
//    @Id
//    @GeneratedValue
//    private long id;
    private String tempId;
    
    private String groupId;
    
    private String cpyId;
    private String userId;
    
    private String parentId;
    
    private String status;
    
    //计算Id
    private String pId;
    
    //一级表头
//    private String firstfieldName;
//    //二级表头
//    private String fieldName;
//    
//    private String fieldCode;

//    //没有用到
//    private String refId;
//    
//    //工资项分类
//    private String itemClass;
//    //小数点位数
//    private String point;
//    
//    //默认值
//    private String defaultValue;
    
    //引用数据
    private String reffieldColumn;
    private String reffieldTable;
    
    private String fieldName;
    private String fieldType;
    private String fieldCode;
    private String fieldValue;
    
    //0-引用   1-导入   2-公式
    private String rulesType;
    //计算公式，可以按列名或者 列的顺序
    @Column(length = 5000)
    private String rules;
    //规则描述
    private String rulesDesc;
    
    //顺序
    private String orderDesc;
    //列表是否显示
    private String isColumnView;
    //税前税后款项
    private String isTax;
    //工资单是否显示
    private String isSalaryBill;
    
    @Column(length = 5000)
    private  String json;
    
    
}
