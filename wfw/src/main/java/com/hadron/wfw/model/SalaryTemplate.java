package com.hadron.wfw.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * User
 * <p/>
 * Created in 2018.07.25
 * <p/>
 *
 * @author <a href="https://github.com/liaozihong" style="background: #55a7e3;">Liaozihong</a>
 */
@Entity
@Table(name = "t_salary_template")
@Data
@AllArgsConstructor
@NoArgsConstructor 
public class SalaryTemplate extends BaseEntity implements Serializable{
//    @Id
//    @GeneratedValue
//    private long id;
    //基本字段
    private String tName;
    
    private String cpyId;
    //公司名字
    private String company;
    
    private String userId;
    
    
    //部门Id
    private String orgId;
    
    //* 计薪周期 当月1日   至当月最后一天
   // 
    private String circle;
    
    //计薪月份
    private String month;
    
    //发薪日
    private String payDay;
    
    //计薪时长
    private String hoursOfdailyPay;
    
    //模板状态
    private String status;

    //动态扩展字段
    //算薪人员清单
    private String includeList;
    //排除人员清单
    private String excluSionList;
}
