package com.hadron.wfw.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * User
 * <p/>
 * Created in 2018.07.25
 * <p/>
 *
 * @author <a href="https://github.com/liaozihong" style="background: #55a7e3;">Liaozihong</a>
 */
@Entity
@Table(name = "t_salary_report")
@Data
@AllArgsConstructor
@NoArgsConstructor 
public class SalaryReport {
    @Id
    @GeneratedValue
    private long id;
    //算薪周期
    private String circle;
    //算薪月份
    private String month;
    //计薪时长
    private String duration;
    //发薪日
    private String payDate;
    //企业Id
    private String cpyId;
    //操作人员id
    private String userId;
    //模板ID
    private String templateId;
    //进程名称
    private String pName;
    //进程状态
    private String status;
    //当前处理阶段
    private String stage;
    //下个处理阶段
    private String next_action;
    //前序处理 阶段
    private String pre;
    //是否锁定
    private String islock;
    //审批状态
    private String approve;
     //当前计薪人数
    private int count;
    //入职人数
    private int entry;
    //转正人数
    private int promotion;
    //离职人数
    private int quit;
    //定薪人数
    private int determine;
    //未定薪人数
    private int undetermine;
    //调薪人数
    private int adjust;
}
