package com.hadron.wfw.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * User
 * <p/>
 * Created in 2018.07.25
 * <p/>
 *
 * @author <a href="https://github.com/liaozihong" style="background: #55a7e3;">Liaozihong</a>
 */
@Entity
@Table(name = "t_personal_deduct")
@Data
@AllArgsConstructor
@NoArgsConstructor 
public class SalaryPersonalDeduct extends BaseEntity implements Serializable {

    //算薪周期
    private String   cpyId;
    
    private String   userId;
    
    private String   idCardNum;
    
    private String   userName;

    
    private double  childEdu;
    
    private double  audltEdu;
    
    private double  bigMedical;
    
    private double  houseLoans;
    
    private double  houseRent;
    
    private double  supportOld;
    
    
    private double  totalTax;
    
    //计薪时长

}
