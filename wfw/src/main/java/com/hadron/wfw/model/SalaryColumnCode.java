package com.hadron.wfw.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * User
 * <p/>
 * Created in 2018.07.25
 * <p/>
 *  @Table(name = "UserOrgUnit",uniqueConstraints = @UniqueConstraint(columnNames = {"resModule","userId", "orgUnitId"}
 *  https://www.cnblogs.com/shiqiboy3974/p/14843394.html
 *  
 *  @Table(uniqueConstraints = {
        @UniqueConstraint(columnNames={"name", "del"}),
        @UniqueConstraint(columnNames={"account", "del"})
})
 *  
 * @author <a href="https://github.com/liaozihong" style="background: #55a7e3;">Liaozihong</a>
 */
@Entity
//@Table(name = "t_salary_columncode",uniqueConstraints=@UniqueConstraint(columnNames="columnName"))
@Table(name = "t_salary_columncode",uniqueConstraints = {
        @UniqueConstraint(columnNames={"columnName", "dataType"}),
        //@UniqueConstraint(columnNames={"account", "del"})
})
@Data
@AllArgsConstructor
@NoArgsConstructor 
//@Table(name = "tableName",uniqueConstraints=@UniqueConstraint(columnNames="A,B"))
public class SalaryColumnCode  extends BaseEntity implements Serializable{

    //算薪周期
    private String  cnName;
    //参保城市
    private String  columnName;
    //1-人事 ，2- 考勤 ，3-社保公积金    
    private String  dataType;
    //公司id
}
