package com.hadron.wfw.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * User
 * <p/>
 * Created in 2018.07.25
 * <p/>
 *
 * @author <a href="https://github.com/liaozihong" style="background: #55a7e3;">Liaozihong</a>
 */
@Entity
@Table(name = "t_salary_insuranceplan")
@Data
@AllArgsConstructor
@NoArgsConstructor 
public class SalaryInsurancePlan  extends BaseEntity implements Serializable{
    //参保城市
    private String  city;
    
    //参保城市
    private String  cityName;
    
    //公司id
    private String  cpyId;
    //社保公司比例

    //社保公司比例
    private float  oldageCompanyRatio;
    
    //社保个人比例
    private float  oldagePersonalRatio;
    
    //失业
    private float    unemploymentCompanyRatio;
    //社保个人比例
    private float    unemploymentPersonalRatio;
    
    //生育
    private float   birthCompanyRatio;
    //社保个人比例
    private float   birthPersonalRatio;
    //工伤
    private float    injuryCompanyRatio;
    //社保个人比例
    private float    injuryPersonalRatio;
    
    private float    medicalCompanyRatio;
    //社保个人比例
    private float    medicalPersonalRatio;
    
//    //社保公司比例
//    private float  socialCompanyRatio;
//    //社保个人比例
//    private float  socialPersonalRatio;
    
    //住房公积金公司比例    
    private float  houseFundsCompanyRatio;
    //住房公积金公司比例
    private float  houseFundsPersonalRatio;


}
