package com.hadron.wfw.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * User
 * <p/>
 * Created in 2018.07.25
 * <p/>
 *
 * @author <a href="https://github.com/liaozihong" style="background: #55a7e3;">Liaozihong</a>
 */
@Entity
@Table(name = "t_platform_company")
@Data
@AllArgsConstructor
@NoArgsConstructor 
public class PlatformCompany   extends BaseEntity implements Serializable {

    //公司名字
    private String  companyName;
    //地址
    private String  address;
    //注册资金
    private String  regFunds;
    //类型
    private String  type;
       //经营范围
    private String  scope;
   //社会信用号
    private String  socialCreditNum;
    //成立时间
    private String  setupDate;
    //经营期限
    private String  businessTerm;
    //登记机关
    private String  regAuthority;
    //开户行
    private String  bankName;
    //对公账号
    private String  bankAccount;
    //法人代表
    private String  legalPerson;
    //营业执照文件
    private String  businessLicense;
     //税号
    private String  TaxNum;
    //计薪时长

}
