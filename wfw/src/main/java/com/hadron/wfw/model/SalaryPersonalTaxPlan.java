package com.hadron.wfw.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * User
 * <p/>
 * Created in 2018.07.25
 * <p/>
 *
 * @author <a href="https://github.com/liaozihong" style="background: #55a7e3;">Liaozihong</a>
 */
@Entity
@Table(name = "t_salary_personal_taxplan")
@Data
@AllArgsConstructor
@NoArgsConstructor 
public class SalaryPersonalTaxPlan  extends BaseEntity implements Serializable{
    //参保城市
    //private String  city;
    
    //税率表
    private String  taxRateName;
    //社保公司比例

    //级数
    private String  level;
    
    //级数
    private String  json;
    
    //累计预扣和预缴应纳税所得额
    private float  minPayable;
    
    //累计预扣和预缴应纳税所得额
    private float  maxPayable;
    
    //适用税率
    private float  taxRatio;
    
    //速算扣除数
    private float    debductNum;


}
