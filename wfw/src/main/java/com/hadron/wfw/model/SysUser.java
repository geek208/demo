package com.hadron.wfw.model;


import lombok.*;

import javax.persistence.*;


import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "t_wfw_user")
public class SysUser extends BaseEntity implements Serializable {
 
    //@Column(unique = true)
    private String email;
    private String phoneNum;
    //@Column(nullable = false)
    private String password;
    private String nickName;
    //@Column(nullable = false)
    
    //认证身份 角色   admin / user 
    private String role;
    @Column(unique = true)
    private String userName;
    //工号
    private String jobNum;
    
    //所在公司名字
    private String company;
    
    //员工状态  1 已注册，2 试用，3，正式，4 离职
    private String status ="1";
    private String remark;
    
    //公司id
    private String cpyId;
    
    //角色id
    private String roleId;
    //部门id
    private String orgId;
    //岗位Id
    private String positionId;
    //职位ID
    private String jobId;
    //职级
    private String levelId;

    @Transient
    private String vCode;
    //我被邀请的码
    @Column
    private  String inviteCode="88888";
    //我的邀请码
    private  String myInvite;
    
    //身份证号
    private  String idCardNum;
    //银行卡号
    private  String bankCardNum;
    private  String bankName;
    
    //社保号
    private  String socialSecurityNum;
    //社保卡号
    private  String socialSecurityCardNum;
    
    //公积帐号
    private  String houseFundNum;
    //公积卡号
    private  String houseFundCardNum;
    //住址
    private  String address;
    //基本工资
    private  Double   baseSalary;
    //浮动工资
    private  Double   floatSalary;
    //补贴
    private  Double   allowance;
    //奖金
    private  Double   bonus;
    
    @Column(length = 5000)
    private  String json;
    
    @Column(length = 5000)
    private  String salary;
    
    
    public UserVO toVO() {
        UserVO userVO = super.toVO(UserVO.class);
        userVO.setPassword(null);
        return userVO;
    }


}
