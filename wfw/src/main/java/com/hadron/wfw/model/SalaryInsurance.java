package com.hadron.wfw.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * User
 * <p/>
 * Created in 2018.07.25
 * <p/>
 *
 * @author <a href="https://github.com/liaozihong" style="background: #55a7e3;">Liaozihong</a>
 */
@Entity
@Table(name = "t_salary_insurance")
@Data
@AllArgsConstructor
@NoArgsConstructor 
public class SalaryInsurance  extends BaseEntity implements Serializable{

    //算薪周期
    private String  name;
    //参保城市
    private String  city;
    
    //公司id
    private String  cpyId;
    
    //公司id
    private String  userId;
    
    //缴交基数
    private double  basefee;
    
    //社保公司比例
    private double  oldageCompanyNum;
    
    //社保个人比例
    private double  oldagePersonalNum;

    
    //失业
    private double    unemploymentCompanyNum;
    //社保个人比例
    private double    unemploymentPersonalNum;
    
    //生育
    private double   birthCompanyNum;
    //社保个人比例
    private double   birthPersonalNum;
    //工伤
    private double    injuryCompanyNum;
    //社保个人比例
    private double    injuryPersonalNum;
    
    private double    medicalCompanyNum;
    //社保个人比例
    private double    medicalPersonalNum;

    
    //住房公积金公司比例    
    private double  houseFundsCompanyNum;
    //住房公积金公司比例
    private double  houseFundsPersonalNum;
    
    private double  totalCompany;
    private double  totalPerson;
    private double  total;
    
    @Column(length = 5000)
    private  String json;


}
