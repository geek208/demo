package com.hadron.wfw.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;




/**
 * User
 * <p/>
 * Created in 2018.07.25
 * <p/>
 *
 * @author <a href="https://github.com/liaozihong" style="background: #55a7e3;">Liaozihong</a>
 */
@Entity
@Table(name = "t_salary_attendance")
@Data
@AllArgsConstructor
@NoArgsConstructor 
public class SalaryAttendance extends BaseEntity implements Serializable{

	
	private String cpyId;
	
	private String userId;
	
    //缺勤天数   =应出勤-实际出勤
    private float queqin;
    
    //迟到
    private float  chidao;
    
    //早退
    private float  zaotui;
    
    //出勤天数   22
    private float   yingchuqin =22;
    
    //实际出勤天数  20 
    private float   shijichuqin;
    
    //出勤天数  20 
    //private int   qingjia;
    
    //旷工
    private float  kuanggong;
    //病假
    private float  bingjia;
    //婚假
    private float  shijia;
    //婚假
    private float  hunjia;
    //丧假
    private float  sangjia;
    //出差天数
    private float  chuchai;
    //加班天数
    private float  jiaban;
    //计薪时长
    
    @Column(length = 5000)
    private  String json;

}
